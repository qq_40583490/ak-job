# AkJob

## 概述

欢迎来到AkJob的开源项目页面！本项目致力于为用户提供一个高效、灵活且易于集成的任务调度解决方案。通过提供简洁明了的API和详尽的文档支持，我们希望促进社区参与，共同打造更强大的调度工具。

`gitcode:`  [gitcode.com/qq_40583490/ak-job](https://gitcode.com/qq_40583490/ak-job)

`gitee:` [gitee.com/lilu777/ak-job](https://gitee.com/lilu777/ak-job)

> 目前为G-Star 孵化项目，您的star是对我们最大的支持！

## 对比

|                  |     **Xxl-Job**      |               **AkJob**               |
| ----------------|----------------------|----------------------------------------|
|  **发布日期**     | 2015 | 2025 |
|  **前后端分离**     | 否  | 是 |
|  **技术架构**     | spring mvc + jsp  | spring boot + Layui |
|  **后台管理**     |     有                | 有                                     |
|  **集群**     |     支持                | 支持                                     |
|  **异常恢复**    | 支持         | 支持      |
|  **定时策略**     | Cron、固定频率、父子依赖 | Cron |
|  **任务配置**     |     客户端编码+页面手动配置    | 客户端编码后自动同步                       |
|  **通信协议**     |     HTTP           | RMI                       |
|  **多环境配置**     |     不支持           | 支持                       |
|  **广播**     |     支持           | 支持                       |
|  **脚本任务**     |     支持           | 暂不支持                       |
|  **保存执行快照**  |     无                | 有                                    |
|  **失败重试**     |     有                | 无                                     |
|  **监控告警**     |     邮件               | 邮件                                   |
|  **查看执行日志**  |     有                | 有                                   |
|  **统计**  |     有                | 有                                   |

## 快速体验

想要快速了解系统功能？请访问我们的[job.ankao.net](https://job.ankao.net/)。使用预设账户`demo`（密码：`123456`）登录，探索系统的核心特性。

### 界面预览

我们为调度中心设计了直观易用的管理界面，下面是一些关键页面的截图：
![角色管理](doc/role.png)
![命名空间](doc/namespace.png)
![应用管理](doc/yingyong.png)
![任务管理](doc/job.png)

## 服务端配置

### 基础设置

在`application-local.yml`中定义了关键的服务端配置参数：

```yaml
server:
  port: 9053 # HTTP服务端口

ak:
  user:
    secretKey: "0000928b4c1e408db9d7651d89b1fe2b" # 密码加密密钥
  db:
    mysql:
      ip: "<MySQL服务器IP>"
      port: "<MySQL端口>"
      name: "<数据库名>"
      username: "<用户名>"
      password: "<密码>"
  job:
    server:
      port: 9001 # RMI服务端口，不能与HTTP端口相同
    email:
      smtp-host: "<SMTP服务器地址>"
      login-name: "<邮箱登录名>"
      auth-code: "<授权码/密码>"
```

> **注意**：请确保替换所有占位符为您的实际配置信息。

## 客户端集成

### Maven依赖

为了简化客户端集成，我们提供了基于Spring Boot的starter库。只需添加以下依赖到您的`pom.xml`文件中：

```xml

<dependency>
    <groupId>net.ankao</groupId>
    <artifactId>ak-job-client-starter</artifactId>
    <version>1.0-RELEASE</version>
</dependency>
```

### 配置示例

以下是客户端的基本配置示例，您可以根据需要进行调整：

```yaml
ak:
  job:
    namespaceId: public # 命名空间
    applicationName: ${spring.application.name} # 应用code，默认取自Spring应用名称
    applicationDes: "排班小程序服务端" # 应用描述
    corePoolSize: 2 # 核心线程数，默认5
    clientIp: 172.27.80.142 # 客户端IP，默认获取内网IP
    clientPort: 9898 # 客户端RMI端口，默认9898
    server:
      url: 172.27.80.142 # 调度中心地址
      port: 9001 # 调度中心RMI端口
```

## 任务实现指南

创建一个简单的定时任务只需要继承`AnkaoJob`接口，并实现相应的方法。例如：

```java
import net.ankao.job.starter.client.common.AnkaoJob;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DemoJob implements AnkaoJob {
    @Override
    public String defaultJobName() {
        return "定时任务小Demo";
    }

    @Override
    public String defaultCronStr() {
        return "* * * * * ?"; // 每秒钟执行一次
    }

    @Override
    public void doTask(Map<String, String> params) {
        System.out.println("我在执行了");
    }
}
```

## 后续计划

未来我们将推出非Spring版本的客户端，以满足更多场景的需求。同时，我们也期待社区成员贡献代码、提出建议或报告问题，一起推动项目的进步。

> 您的star是对我们最大的支持！
