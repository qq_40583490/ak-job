package net.ankao.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.ankao.job.entity.AkNamespace;
import net.ankao.job.entity.dto.NamespaceListDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 命名空间 Mapper 接口
 * </p>
 *
 * @author LILU
 * @since 2025-01-16
 */
public interface AkNamespaceMapper extends BaseMapper<AkNamespace> {

    /**
     * 明细列表
     * @param namespaceListDTO
     * @return
     */
    List<AkNamespace> infoList(@Param("namespaceListDTO") NamespaceListDTO namespaceListDTO);

}
