package net.ankao.job.mapper;

import net.ankao.job.entity.AkApplicationJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.ankao.job.entity.dto.JobListDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Job表 Mapper 接口
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface AkApplicationJobMapper extends BaseMapper<AkApplicationJob> {

    List<AkApplicationJob> infoList(@Param("applicationId")String applicationId,@Param("jobUpdateDTO") JobListDTO jobUpdateDTO);

}
