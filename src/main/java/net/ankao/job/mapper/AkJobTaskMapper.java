package net.ankao.job.mapper;

import net.ankao.job.entity.AkJobTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface AkJobTaskMapper extends BaseMapper<AkJobTask> {

}
