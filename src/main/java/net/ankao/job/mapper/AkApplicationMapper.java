package net.ankao.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.dto.ApplicationListDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 应用 Mapper 接口
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface AkApplicationMapper extends BaseMapper<AkApplication> {

    /**
     * 详情分页列表
     *
     * @param namespaceId
     * @param applicationListDTO
     * @return
     */
    List<AkApplication> infoList(@Param("namespaceId") String namespaceId, @Param("params") ApplicationListDTO applicationListDTO);


}
