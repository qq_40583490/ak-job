package net.ankao.job.mapper;

import net.ankao.job.entity.AkApplicationClient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用客户端 Mapper 接口
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface AkApplicationClientMapper extends BaseMapper<AkApplicationClient> {

}
