package net.ankao.job.common;

import net.ankao.job.common.route.ClientRouteStrategy;
import net.ankao.job.entity.AkApplicationClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author LILU
 * @date 2024-12-04 9:20
 */
public class ClientRouterManager {

    private static HashMap<Integer, ClientRouteStrategy> STRATEGY_HASH_MAP = new HashMap<>();

    public static void putClientRouteStrategy(ClientRouteStrategy clientRouteStrategy) {
        if (null == clientRouteStrategy.strategyCode()) {
            return;
        }
        STRATEGY_HASH_MAP.put(clientRouteStrategy.strategyCode(), clientRouteStrategy);
    }

    public static List<AkApplicationClient> getClient(Integer executiveStrategy, List<AkApplicationClient> akApplicationClientList) {
        ClientRouteStrategy clientRouteStrategy = STRATEGY_HASH_MAP.get(executiveStrategy);
        if (clientRouteStrategy == null || akApplicationClientList.isEmpty()) {
            return new ArrayList<>();
        }
        List<AkApplicationClient> route = clientRouteStrategy.route(akApplicationClientList);
        return route;
    }

}
