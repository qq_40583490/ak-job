package net.ankao.job.common.route;

import net.ankao.job.entity.AkApplicationClient;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * 随机策略
 * @author LILU
 * @date 2024-12-04 9:13
 */
@Component
public class RandomStrategy implements ClientRouteStrategy {

    private Random random = new Random();

    @Override
    public Integer strategyCode() {
        return 4;
    }

    @Override
    public List<AkApplicationClient> route(List<AkApplicationClient> clients) {
        List<AkApplicationClient> availableClients = clients.stream()
                .filter(client -> client.getStatus() == 1)
                .collect(Collectors.toList());

        if (availableClients.isEmpty()) {
            return null;
        }

        int index = random.nextInt(availableClients.size());
        return Arrays.asList(availableClients.get(index));
    }
}