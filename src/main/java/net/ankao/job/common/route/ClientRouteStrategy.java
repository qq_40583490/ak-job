package net.ankao.job.common.route;

import net.ankao.job.entity.AkApplicationClient;

import java.util.List;

/**
 * @author LILU
 * @date 2024-12-04 9:08
 */
public interface ClientRouteStrategy {

    Integer strategyCode();

    /**
     * 路由策略实现
     * @param clients
     * @return
     */
    List<AkApplicationClient> route(List<AkApplicationClient> clients);

}
