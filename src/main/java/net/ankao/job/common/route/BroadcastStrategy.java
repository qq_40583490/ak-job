package net.ankao.job.common.route;

import net.ankao.job.entity.AkApplicationClient;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 广播
 * @author LILU
 * @date 2024-12-04 9:29
 */
@Component
public class BroadcastStrategy implements ClientRouteStrategy {

    @Override
    public Integer strategyCode() {
        return 7;
    }

    @Override
    public List<AkApplicationClient> route(List<AkApplicationClient> clients) {
        return clients.stream()
                .filter(client -> client.getStatus() == 1)
                .collect(Collectors.toList());
    }

}
