package net.ankao.job.common.route;

import net.ankao.job.entity.AkApplicationClient;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * 最后一个
 *
 * @author LILU
 * @date 2024-12-04 9:09
 */
@Component
public class LastClientStrategy implements ClientRouteStrategy {
    @Override
    public Integer strategyCode() {
        return 3;
    }

    @Override
    public List<AkApplicationClient> route(List<AkApplicationClient> clients) {
        return Arrays.asList(clients.stream()
                .filter(client -> client.getStatus() == 1)
                .reduce((first, second) -> second)
                .orElse(null));
    }
}
