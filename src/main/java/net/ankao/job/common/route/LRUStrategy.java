package net.ankao.job.common.route;

import net.ankao.job.entity.AkApplicationClient;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 最近最久未使用策略
 * @author LILU
 * @date 2024-12-04 9:15
 */
public class LRUStrategy implements ClientRouteStrategy {

    // 为每个applicationId维护独立的最后使用时间
    private Map<String, Map<String, LocalDateTime>> lastUsedTimeMap = new ConcurrentHashMap<>();

    @Override
    public Integer strategyCode() {
        return 6;
    }

    @Override
    public List<AkApplicationClient> route(List<AkApplicationClient> clients) {
        if (clients == null || clients.isEmpty()) {
            return null;
        }

        String applicationId = clients.get(0).getApplicationId();
        Map<String, LocalDateTime> lastUsedTime = lastUsedTimeMap.computeIfAbsent(applicationId, k -> new ConcurrentHashMap<>());

        return Arrays.asList(clients.stream()
                .filter(client -> client.getStatus() == 1)
                .min(Comparator.comparing(client ->
                        lastUsedTime.getOrDefault(client.getId(), LocalDateTime.MIN)))
                .map(client -> {
                    lastUsedTime.put(client.getId(), LocalDateTime.now());
                    return client;
                })
                .orElse(null));
    }
}
