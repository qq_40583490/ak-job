package net.ankao.job.common;

/**
 * @author LILU
 * @date 2024-11-28 9:38
 */
public interface Constant {

    String SERVER_GROUP = "serverGroup";

    String SERVER_CLASS_NAME = "IAkJobServerService";

    String CLIENT_CLASS_NAME = "IAkJobClientService";

    String APPLICATION_JOB_KEY = "_applicationJob";

    String TIME_OUT_STR = "_timeOut";

    String APPLICATION_LIST = "application:list";

    String APPLICATION_UPDATE = "application:update";

    String JOB_LIST = "job:list";

    String JOB_UPDATE = "job:update";

    String TASK_INFO = "task:info";

    String NAMESPACE_INFO = "namespace:info";

    String NAMESPACE_UPDATE = "namespace:update";

    String NAMESPACE_ADD = "namespace:add";

    String NAMESPACE_DEL = "namespace:del";
}
