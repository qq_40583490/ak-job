package net.ankao.job.common;

import lombok.Data;

/**
 * @author LILU
 * @date 2024-11-28 14:05
 */
@Data
public class ClientJobResult {

    /**
     * 1成功 0失败 -1超时
     */
    private Integer status;

    private String msg;

    public ClientJobResult(Integer status) {
        this.status = status;
    }

    public ClientJobResult(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }
}
