package net.ankao.job.common;

import lombok.extern.slf4j.Slf4j;
import net.ankao.job.core.rpc.IAkJobClientService;
import net.ankao.job.entity.AkApplicationClient;
import net.ankao.job.entity.AkApplicationJob;
import org.springframework.stereotype.Component;

import java.rmi.Naming;
import java.util.HashMap;
import java.util.List;

/**
 * @author LILU
 * @date 2024-11-28 10:14
 */
@Slf4j
@Component
public class JobNamingManager {

    private HashMap<String, IAkJobClientService> namingMap = new HashMap<>();

    public IAkJobClientService getNaming(String clientId) {
        return namingMap.get(clientId);
    }

    public void delNaming(String clientId) {
        namingMap.remove(clientId);
    }

    public void addNaming(AkApplicationClient akApplicationClient) {
        IAkJobClientService jobClientService = null;
        try {
            jobClientService = (IAkJobClientService) Naming.lookup("rmi://" + akApplicationClient.getClientUrl() + ":" + akApplicationClient.getClientPort() + "/" + Constant.CLIENT_CLASS_NAME);
            namingMap.put(akApplicationClient.getId(),jobClientService);
        } catch (Exception e) {
//            log.warn(e.getMessage(),e);
        }
    }

}
