package net.ankao.job.utils;

import org.quartz.CronExpression;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * @author LILU
 * @date 2025-01-15 14:49
 */
public class CronUtils {

    public static List<String> lastExecutionTime(String cron) {
        List<String> list = new ArrayList<>();
        try {
            if (CronExpression.isValidExpression(cron)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                CronExpression cronExpr = new CronExpression(cron);
                // 设置时区（可选），如果没有设置，默认使用系统时区
                cronExpr.setTimeZone(TimeZone.getDefault());
                Date nextExecution = new Date(); // 当前时间
                for (int i = 0; i < 5; i++) {
                    nextExecution = cronExpr.getNextValidTimeAfter(nextExecution);
                    if (nextExecution != null) {
                        list.add( sdf.format(nextExecution));
                        nextExecution.setTime(nextExecution.getTime());
                    } else {
                        break;
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 验证cron表达式是否合法
     * @param cron
     * @return
     */
    public static Boolean isValidExpression(String cron){
        return CronExpression.isValidExpression(cron);
    }

}
