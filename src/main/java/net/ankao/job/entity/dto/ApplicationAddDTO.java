package net.ankao.job.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author LILU
 * @date 2024-12-04 13:32
 */
@Data
public class ApplicationAddDTO {

    @NotEmpty
    private String namespaceId;

    /**
     * 应用名
     */
    private String applicationName;

    /**
     * 应用描述
     */
    private String applicationDes;



}
