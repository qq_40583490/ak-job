package net.ankao.job.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import net.ankao.framework.starter.base.domain.model.PageModel;

/**
 * @author LILU
 * @date 2024-12-04 13:32
 */
@Data
public class ApplicationListDTO extends PageModel {

    /**
     * 应用名
     */
    private String applicationName;

    /**
     * 应用描述
     */
    private String applicationDes;


}
