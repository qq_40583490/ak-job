package net.ankao.job.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author LILU
 * @date 2024-12-04 14:07
 */
@Data
public class ApplicationClientListDTO {

    @NotEmpty
    private String applicationId;

}
