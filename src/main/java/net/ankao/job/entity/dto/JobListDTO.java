package net.ankao.job.entity.dto;

import lombok.Data;
import net.ankao.framework.starter.base.domain.model.PageModel;

import javax.validation.constraints.NotEmpty;

/**
 * @author LILU
 * @date 2024-12-04 13:32
 */
@Data
public class JobListDTO extends PageModel {

    /**
     * job描述
     */
    private String name;

    /**
     * 任务状态，1 启动，0 未启动
     */
    private Integer status;


}
