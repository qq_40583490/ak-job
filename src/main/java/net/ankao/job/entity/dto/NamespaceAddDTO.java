package net.ankao.job.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author LILU
 * @date 2025-01-16 16:10
 */
@Data
public class NamespaceAddDTO {

    private String id;

    /**
     * 名称
     */
    @NotEmpty
    private String name;

    /**
     * 描述
     */
    private String namespaceDes;

}
