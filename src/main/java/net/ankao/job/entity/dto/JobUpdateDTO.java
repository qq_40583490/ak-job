package net.ankao.job.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * Job表
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class JobUpdateDTO extends Model<JobUpdateDTO> {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * job描述
     */
    private String name;

    /**
     * 执行方法类
     */
    private String beanName;

    /**
     * cron表达式
     */
    private String cronStr;

    /**
     * 执行策略
     */
    private Integer executiveStrategy;

    /**
     * 默认执行参数
     */
    private String paramsStr;

    /**
     * 秒
     */
    private Long timeOut;

    /**
     * 重试次数
     */
    private Integer retryNum;

    /**
     * 责任人
     */
    private String managerName;

    /**
     * 责任人邮件地址
     */
    private String managerMailAddress;

    /**
     * 每小时最大发送次数
     */
    private Integer maxEmailsPerHour;
}
