package net.ankao.job.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import net.ankao.framework.starter.base.domain.model.PageModel;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author LILU
 * @date 2024-12-04 13:58
 */
@Data
public class JobTaskListDTO extends PageModel {

    /**
     * 任务执行开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 任务执行结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    private String remark;

    /**
     * 状态 1成功 0失败 -1超时
     */
    private Integer status;

}
