package net.ankao.job.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * job执行DTO
 *
 * @author LILU
 * @date 2025-01-16 9:21
 */
@Data
public class JobExecuteDTO {

    @NotNull
    private String id;

    @ApiModelProperty("参数")
    private String paramsStr;

}
