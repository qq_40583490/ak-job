package net.ankao.job.entity.dto;

import lombok.Data;
import net.ankao.framework.starter.base.domain.model.PageModel;

/**
 * @author LILU
 * @date 2025-01-16 16:10
 */
@Data
public class NamespaceListDTO extends PageModel {

    private String id;

    private String name;

}
