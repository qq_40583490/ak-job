package net.ankao.job.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 命名空间
 * </p>
 *
 * @author LILU
 * @since 2025-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ak_namespace")
public class AkNamespace extends Model<AkNamespace> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;


    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 描述
     */
    @TableField("namespace_des")
    private String namespaceDes;

    @TableField(exist = false)
    private Integer applicationCount;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
