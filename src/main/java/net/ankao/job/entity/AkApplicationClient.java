package net.ankao.job.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 应用客户端
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ak_application_client")
public class AkApplicationClient extends Model<AkApplicationClient> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 应用id
     */
    @TableField("application_id")
    private String applicationId;

    /**
     * 应用名
     */
    @TableField("application_name")
    private String applicationName;

    /**
     * 应用ip
     */
    @TableField("client_url")
    private String clientUrl;

    /**
     * 应用端口
     */
    @TableField("client_port")
    private Integer clientPort;

    /**
     * 注册时间
     */
    @TableField("join_time")
    private LocalDateTime joinTime;

    /**
     * 最后一次心跳时间
     */
    @TableField("last_heartbeat_time")
    private LocalDateTime lastHeartbeatTime;

    /**
     * 服务状态 1 在线，0下线
     */
    @TableField("status")
    private Integer status;

    /**
     * 命名空间id
     */
    @TableField("namespace_id")
    private String namespaceId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
