package net.ankao.job.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 应用
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ak_application")
public class AkApplication extends Model<AkApplication> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 命名空间
     */
    @TableField("namespace_id")
    private String namespaceId;

    /**
     * 应用名
     */
    @TableField("application_name")
    private String applicationName;

    /**
     * 应用描述
     */
    @TableField("application_des")
    private String applicationDes;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 客户端数量
     */
    @TableField(exist = false)
    private Integer clientCount;

    /**
     * 在线客户端数量
     */
    @TableField(exist = false)
    private Integer onLineClientCount;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
