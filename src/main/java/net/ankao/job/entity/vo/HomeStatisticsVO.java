package net.ankao.job.entity.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import net.ankao.job.common.CustomDoubleSerializer;

/**
 * @author LILU
 * @date 2025-01-17 13:41
 */
@Data
public class HomeStatisticsVO {

    /**
     * 应用数量
     */
    private Long applicationCount;

    /**
     * 任务数量
     */
    private Long jobCount;

    /**
     * 调度次数
     */
    private Long taskCount;

    /**
     * 成功数量
     */
    private Long successCount;

    /**
     * 超时数量
     */
    private Long timeoutCount;

    /**
     * 失败数量
     */
    private Long failCount;

    /**
     * 成功率
     */
    @JsonSerialize(using = CustomDoubleSerializer.class)
    private Double successRate;

    public Double getSuccessRate() {
        if(taskCount == 0){
            return null;
        }
        return 100D * successCount/taskCount;
    }
}
