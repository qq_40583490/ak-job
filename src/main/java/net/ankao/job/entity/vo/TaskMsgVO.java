package net.ankao.job.entity.vo;

import lombok.Builder;

/**
 * @author LILU
 * @date 2025-01-20 10:02
 */
@Builder
public class TaskMsgVO {

    private String namespaceId;

    private String namespaceName;

    private String applicationName;

    private String jobName;

    private String status;

    private String dateTime;

    private String clientPath;

    private String errorMsg;

    @Override
    public String toString() {
        return "命名空间='" + namespaceName + '\'' + "<br>"+
                " 命名空间ID='" + namespaceId + '\'' + "<br>"+
                " 应用名称='" + applicationName + '\'' + "<br>"+
                " 任务名称='" + jobName + '\'' + "<br>"+
                " 调度时间='" + dateTime + '\'' + "<br>"+
                " 调度节点='" + clientPath + '\'' + "<br>"+
                " 执行状态='" + status + '\'' + "<br>"+
                " 错误日志='" + errorMsg + '\'';
    }
}
