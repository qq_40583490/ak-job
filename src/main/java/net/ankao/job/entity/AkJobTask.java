package net.ankao.job.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ak_job_task")
public class AkJobTask extends Model<AkJobTask> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @TableField("job_id")
    private String jobId;

    /**
     * 状态 1成功 0失败 -1超时
     */
    @TableField("status")
    private Integer status;

    /**
     * 错误日志
     */
    @TableField("error_log")
    private String errorLog;

    /**
     * 应用ip
     */
    @TableField("client_url")
    private String clientUrl;

    /**
     * 应用端口
     */
    @TableField("client_port")
    private Integer clientPort;

    /**
     * 任务开始时间
     */
    @TableField("start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 任务结束时间
     */
    @TableField("end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 默认执行参数
     */
    @TableField("params_str")
    private String paramsStr;

    /**
     * 命名空间id
     */
    @TableField("namespace_id")
    private String namespaceId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
