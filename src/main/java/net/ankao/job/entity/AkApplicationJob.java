package net.ankao.job.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * Job表
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ak_application_job")
public class AkApplicationJob extends Model<AkApplicationJob> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 应用id
     */
    @TableField("application_id")
    private String applicationId;

    /**
     * 应用名
     */
    @TableField("application_name")
    private String applicationName;

    /**
     * job描述
     */
    @TableField("name")
    private String name;

    /**
     * 执行方法类
     */
    @TableField("bean_name")
    private String beanName;

    /**
     * cron表达式
     */
    @TableField("cron_str")
    private String cronStr;

    /**
     * 执行策略
     * 1.第一个； 2.轮询； 3.最后一个； 4.随机； 5.最不经常使用 6.最近最久未使用 7.广播
     */
    @TableField("executive_strategy")
    private Integer executiveStrategy;

    /**
     * 默认执行参数
     */
    @TableField("params_str")
    private String paramsStr;

    /**
     * 秒
     */
    @TableField(value = "time_out",updateStrategy = FieldStrategy.IGNORED)
    private Long timeOut;

    /**
     * 失败重试次数
     */
    @TableField(value = "retry_num",updateStrategy = FieldStrategy.IGNORED)
    private Integer retryNum;

    /**
     * 责任人
     */
    @TableField("manager_name")
    private String managerName;

    /**
     * 责任人邮件地址
     */
    @TableField("manager_mail_address")
    private String managerMailAddress;

    /**
     * 每小时最大发送次数
     */
    @TableField(value = "max_emails_per_hour",updateStrategy = FieldStrategy.IGNORED)
    private Integer maxEmailsPerHour;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 任务状态，1 启动，0 未启动
     */
    @TableField("status")
    private Integer status;

    /**
     * 命名空间id
     */
    @TableField("namespace_id")
    private String namespaceId;

    /**
     * 在线客户端数量
     */
    @TableField(exist = false)
    private Integer onLineClientCount;

    public Integer getMaxEmailsPerHour() {
        return maxEmailsPerHour;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
