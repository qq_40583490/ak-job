package net.ankao.job.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 统计数据
 * </p>
 *
 * @author LILU
 * @since 2025-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ak_statistics")
public class AkStatistics extends Model<AkStatistics> {

    private static final long serialVersionUID = 1L;

    /**
     * 命名空间id
     */
    @TableId("namespace_id")
    private String namespaceId;

    /**
     * 应用数量
     */
    @TableField("application_count")
    private Long applicationCount;

    /**
     * 任务数量
     */
    @TableField("job_count")
    private Long jobCount;

    /**
     * 调度次数
     */
    @TableField("task_count")
    private Long taskCount;

    /**
     * 成功数量
     */
    @TableField("success_count")
    private Long successCount;

    /**
     * 超时数量
     */
    @TableField("timeout_count")
    private Long timeoutCount;

    /**
     * 失败数量
     */
    @TableField("fail_count")
    private Long failCount;


    @Override
    protected Serializable pkVal() {
        return this.namespaceId;
    }

}
