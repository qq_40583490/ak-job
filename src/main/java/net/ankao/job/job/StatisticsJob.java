package net.ankao.job.job;

import lombok.extern.slf4j.Slf4j;
import net.ankao.job.entity.AkStatistics;
import net.ankao.job.service.IAkStatisticsService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LILU
 * @date 2024-11-28 9:22
 */
@Slf4j
public class StatisticsJob implements Job {

    @Resource
    private IAkStatisticsService statisticsService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<AkStatistics> akStatistics = statisticsService.realTimeData();
        for (AkStatistics akStatistic : akStatistics) {
            statisticsService.saveOrUpdate(akStatistic);
        }
    }
}
