package net.ankao.job.startup;

import lombok.extern.slf4j.Slf4j;
import net.ankao.job.common.HeartbeatNamingManager;
import net.ankao.job.common.JobNamingManager;
import net.ankao.job.entity.AkApplicationClient;
import net.ankao.job.service.IAkApplicationClientService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LILU
 * @date 2024-11-28 9:54
 */
@Slf4j
@Component
public class NamingRunner implements CommandLineRunner {

    @Resource
    private IAkApplicationClientService akApplicationClientService;

    @Resource
    private HeartbeatNamingManager heartbeatNamingManager;

    @Resource
    private JobNamingManager jobNamingManager;

    @Override
    public void run(String... args) throws Exception {
        List<AkApplicationClient> list = akApplicationClientService.list();
        for (AkApplicationClient akApplicationClient : list) {
            heartbeatNamingManager.addNaming(akApplicationClient);
//            jobNamingManager.addNaming(akApplicationClient);
        }

    }
}
