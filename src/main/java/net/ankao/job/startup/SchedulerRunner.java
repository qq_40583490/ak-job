package net.ankao.job.startup;

import net.ankao.job.common.ClientJobManager;
import org.quartz.Scheduler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author LILU
 * @date 2024-11-28 10:25
 */
@Component
public class SchedulerRunner implements CommandLineRunner {

    @Resource
    private ClientJobManager clientJobManager;

    @Override
    public void run(String... args) throws Exception {
        clientJobManager.start();
    }
}
