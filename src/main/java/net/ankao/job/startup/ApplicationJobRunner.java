package net.ankao.job.startup;

import net.ankao.job.common.ClientJobManager;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.service.IAkApplicationJobService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LILU
 * @date 2024-11-28 14:29
 */
@Component
public class ApplicationJobRunner implements CommandLineRunner {

    @Resource
    private IAkApplicationJobService applicationJobService;

    @Resource
    private ClientJobManager clientJobManager;

    @Override
    public void run(String... args) throws Exception {
        List<AkApplicationJob> list = applicationJobService.getListByStatus(1);
        for (AkApplicationJob applicationJob : list) {
            clientJobManager.addDynamicJob(applicationJob);
        }
    }
}
