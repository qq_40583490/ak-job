package net.ankao.job.startup;

import net.ankao.job.common.ClientRouterManager;
import net.ankao.job.common.route.ClientRouteStrategy;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LILU
 * @date 2024-12-04 9:49
 */
@Component
public class ClientRouteRunner implements CommandLineRunner {

    @Resource
    private List<ClientRouteStrategy> clientRouteStrategyList;

    @Override
    public void run(String... args) throws Exception {
        if (clientRouteStrategyList == null) {
            return;
        }
        for (ClientRouteStrategy clientRouteStrategy : clientRouteStrategyList) {
            ClientRouterManager.putClientRouteStrategy(clientRouteStrategy);
        }
    }
}
