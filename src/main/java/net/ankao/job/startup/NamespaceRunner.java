package net.ankao.job.startup;

import lombok.extern.slf4j.Slf4j;
import net.ankao.job.service.IAkNamespaceService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author LILU
 * @date 2025-01-17 9:16
 */
@Slf4j
@Component
public class NamespaceRunner  implements CommandLineRunner {

    @Resource
    private IAkNamespaceService namespaceService;

    @Override
    public void run(String... args) throws Exception {
        namespaceService.initPublic();
    }
}
