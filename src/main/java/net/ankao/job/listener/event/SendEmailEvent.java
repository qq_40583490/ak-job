package net.ankao.job.listener.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author LILU
 * @date 2025-01-17 16:38
 */
@Getter
public class SendEmailEvent extends ApplicationEvent {

    /**
     * 收件人
     */
    private String taskId;

    public SendEmailEvent(Object source, String taskId) {
        super(source);
        this.taskId = taskId;
    }

}
