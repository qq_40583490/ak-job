package net.ankao.job.controller;


import io.swagger.annotations.ApiOperation;
import net.ankao.framework.starter.admin.annotation.MenuScan;
import net.ankao.framework.starter.base.common.AkResponse;
import net.ankao.job.common.Constant;
import net.ankao.job.configuration.AppConfig;
import net.ankao.job.entity.AkNamespace;
import net.ankao.job.entity.vo.HomeStatisticsVO;
import net.ankao.job.service.IAkStatisticsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import net.ankao.framework.starter.base.controller.AbstractAkController;

import javax.annotation.Resource;

/**
 * <p>
 * 统计数据 前端控制器
 * </p>
 *
 * @author LILU
 * @since 2025-01-17
 */
@RestController
@RequestMapping("admin/api/akStatistics")
public class AkStatisticsController extends AbstractAkController {

    @Resource
    private IAkStatisticsService akStatisticsService;

    @Resource
    private AppConfig appConfig;

    @ApiOperation("首页统计查询")
    @GetMapping("homeStatistics")
    public AkResponse<HomeStatisticsVO> homeStatistics() {
        HomeStatisticsVO homeStatisticsVO = akStatisticsService.homeStatistics();
        return AkResponse.SUCCESS(homeStatisticsVO);
    }

    @ApiOperation("版本信息")
    @GetMapping("appInfo")
    public AkResponse<AppConfig> appInfo() {
        return AkResponse.SUCCESS(appConfig);
    }



}
