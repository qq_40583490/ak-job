package net.ankao.job.controller;


import io.swagger.annotations.ApiOperation;
import net.ankao.framework.starter.admin.annotation.MenuBelong;
import net.ankao.framework.starter.admin.annotation.MenuScan;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.common.AkResponse;
import net.ankao.framework.starter.base.controller.AbstractAkController;
import net.ankao.job.common.Constant;
import net.ankao.job.entity.AkNamespace;
import net.ankao.job.entity.dto.NamespaceAddDTO;
import net.ankao.job.entity.dto.NamespaceListDTO;
import net.ankao.job.entity.dto.NamespaceUpdateDTO;
import net.ankao.job.service.IAkNamespaceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 命名空间 前端控制器
 * </p>
 *
 * @author LILU
 * @since 2025-01-16
 */
@RestController
@RequestMapping("admin/api/akNamespace")
@MenuScan
public class AkNamespaceController extends AbstractAkController {

    @Resource
    private IAkNamespaceService akNamespaceService;


    @ApiOperation("命名空间分页列表")
    @PostMapping("pageList")
    @MenuBelong(name = Constant.NAMESPACE_INFO, alias = "命名空间列表")
    @RequiresPermissions(Constant.NAMESPACE_INFO)
    public AkResponse<AkPageable<AkNamespace>> pageList(@RequestBody @Validated NamespaceListDTO namespaceListDTO) {
        AkPageable<AkNamespace> akJobTaskAkPageable = akNamespaceService.pageList(namespaceListDTO);
        return AkResponse.SUCCESS(akJobTaskAkPageable);
    }

    @ApiOperation("命名空间查询")
    @GetMapping("info")
    @RequiresPermissions(Constant.NAMESPACE_INFO)
    public AkResponse<AkNamespace> info(@RequestParam String id) {
        AkNamespace akNamespace = akNamespaceService.getById(id);
        return AkResponse.SUCCESS(akNamespace);
    }



    @ApiOperation("命名空间新增")
    @PostMapping("add")
    @MenuBelong(name = Constant.NAMESPACE_ADD, alias = "命名空间新增")
    @RequiresPermissions(Constant.NAMESPACE_ADD)
    public AkResponse<AkNamespace> add(@RequestBody @Validated NamespaceAddDTO namespaceListDTO) {
        AkNamespace akNamespace = akNamespaceService.add(namespaceListDTO);
        return AkResponse.SUCCESS(akNamespace);
    }


    @ApiOperation("命名空间修改")
    @PostMapping("update")
    @MenuBelong(name = Constant.NAMESPACE_UPDATE, alias = "命名空间修改")
    @RequiresPermissions(Constant.NAMESPACE_UPDATE)
    public AkResponse<Boolean> update(@RequestBody @Validated NamespaceUpdateDTO namespaceUpdateDTO) {
        int index = akNamespaceService.updateByDTO(namespaceUpdateDTO);
        return AkResponse.SUCCESS(index > 0);
    }

    @ApiOperation("命名空间删除")
    @PostMapping("del")
    @MenuBelong(name = Constant.NAMESPACE_DEL, alias = "命名空间删除")
    @RequiresPermissions(Constant.NAMESPACE_DEL)
    public AkResponse<Boolean> del(@RequestParam String id) {
        int del = akNamespaceService.del(id);
        return AkResponse.SUCCESS(del > 0);
    }


}
