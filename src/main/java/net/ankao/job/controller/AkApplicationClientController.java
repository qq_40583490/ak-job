package net.ankao.job.controller;


import io.swagger.annotations.ApiOperation;
import net.ankao.framework.starter.admin.annotation.MenuBelong;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.common.AkResponse;
import net.ankao.framework.starter.base.controller.AbstractAkController;
import net.ankao.job.common.Constant;
import net.ankao.job.entity.AkApplicationClient;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.dto.ApplicationClientListDTO;
import net.ankao.job.entity.dto.JobListDTO;
import net.ankao.job.service.IAkApplicationClientService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 应用客户端 前端控制器
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 *
 */
@RestController
@RequestMapping("admin/api/akApplicationClient")
public class AkApplicationClientController extends AbstractAkController {

    @Resource
    private IAkApplicationClientService akApplicationClientService;

    @ApiOperation("应用节点")
    @PostMapping("list")
    @RequiresPermissions(Constant.APPLICATION_LIST)
    public AkResponse<List<AkApplicationClient>> list(@RequestBody @Validated ApplicationClientListDTO applicationClientListDTO) {
        List<AkApplicationClient> list = akApplicationClientService.list(applicationClientListDTO);
        return AkResponse.SUCCESS(list);
    }

}
