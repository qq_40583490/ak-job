package net.ankao.job.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.ankao.framework.starter.admin.annotation.MenuBelong;
import net.ankao.framework.starter.admin.annotation.MenuScan;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.common.AkResponse;
import net.ankao.framework.starter.base.controller.AbstractAkController;
import net.ankao.framework.starter.base.exception.AkException;
import net.ankao.job.common.Constant;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.dto.JobExecuteDTO;
import net.ankao.job.entity.dto.JobListDTO;
import net.ankao.job.entity.dto.JobUpdateDTO;
import net.ankao.job.job.ClientJob;
import net.ankao.job.service.IAkApplicationJobService;
import net.ankao.job.utils.CronUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Job表 前端控制器
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Api(tags = "任务管理")
@RestController
@RequestMapping("admin/api/akApplicationJob")
@MenuScan
public class AkApplicationJobController extends AbstractAkController {

    @Resource
    private IAkApplicationJobService akApplicationJobService;

    @Resource
    private ClientJob clientJob;

    @ApiOperation("任务详情")
    @GetMapping("cron")
    public AkResponse<List<String>> cron(@RequestParam String cron) {
        List<String> list = CronUtils.lastExecutionTime(cron);
        return AkResponse.SUCCESS(list);
    }

    @ApiOperation("任务详情")
    @GetMapping("info")
    @RequiresPermissions(Constant.JOB_LIST)
    public AkResponse<AkApplicationJob> info(@RequestParam String id) {
        AkApplicationJob akApplication = akApplicationJobService.getById(id);
        return AkResponse.SUCCESS(akApplication);
    }

    @ApiOperation("任务列表")
    @PostMapping("pageList/{applicationId}")
    @MenuBelong(name = Constant.JOB_LIST, alias = "任务列表")
    @RequiresPermissions(Constant.JOB_LIST)
    public AkResponse<AkPageable<AkApplicationJob>> pageList(@RequestBody @Validated JobListDTO jobUpdateDTO, @PathVariable String applicationId) {
        AkPageable<AkApplicationJob> akApplicationJobAkPageable = akApplicationJobService.pageList(applicationId, jobUpdateDTO);
        return AkResponse.SUCCESS(akApplicationJobAkPageable);
    }

    @ApiOperation("修改")
    @PostMapping("update")
    @MenuBelong(name = Constant.JOB_UPDATE, alias = "任务管理")
    @RequiresPermissions(Constant.JOB_UPDATE)
    public AkResponse<AkApplicationJob> update(@RequestBody @Validated JobUpdateDTO jobUpdateDTO) {
        AkApplicationJob akApplicationJob = akApplicationJobService.updateByDTO(jobUpdateDTO);
        // 修改后自动关闭
        akApplicationJobService.close(jobUpdateDTO.getId());
        return AkResponse.SUCCESS(akApplicationJob);
    }

    @ApiOperation("启动")
    @GetMapping("start")
    @RequiresPermissions(Constant.JOB_UPDATE)
    public AkResponse<Boolean> start(@RequestParam String id) {
        Boolean start = akApplicationJobService.start(id);
        return AkResponse.SUCCESS(start);
    }

    @ApiOperation("关闭")
    @GetMapping("close")
    @RequiresPermissions(Constant.JOB_UPDATE)
    public AkResponse<Boolean> close(@RequestParam String id) {
        Boolean start = akApplicationJobService.close(id);
        return AkResponse.SUCCESS(start);
    }

    @ApiOperation("执行一次")
    @PostMapping("execute")
    @RequiresPermissions(Constant.JOB_UPDATE)
    public AkResponse<Boolean> execute(@RequestBody @Validated JobExecuteDTO jobExecuteDTO) {
        AkApplicationJob applicationJob = akApplicationJobService.getById(jobExecuteDTO.getId());
        HashMap<String, String> parameter = new HashMap<>();
        try {
            if (StringUtils.hasText(jobExecuteDTO.getParamsStr())) {
                parameter = JSON.parseObject(jobExecuteDTO.getParamsStr(), new TypeReference<Map<String, String>>() {
                });
            } else if (StringUtils.hasText(applicationJob.getParamsStr())) {
                parameter =JSON.parseObject(applicationJob.getParamsStr(), new TypeReference<Map<String, String>>() {
                });
            }
        }catch (Exception e){
            throw new AkException("自定义参数不是有效的JSON格式");
        }
        clientJob.doTask(applicationJob,parameter,"手动执行");
        return AkResponse.SUCCESS(true);
    }


}
