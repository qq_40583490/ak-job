package net.ankao.job.controller;


import io.swagger.annotations.ApiOperation;
import net.ankao.framework.starter.admin.annotation.MenuBelong;
import net.ankao.framework.starter.admin.annotation.MenuScan;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.common.AkResponse;
import net.ankao.framework.starter.base.controller.AbstractAkController;
import net.ankao.job.common.Constant;
import net.ankao.job.entity.AkJobTask;
import net.ankao.job.entity.dto.JobTaskListDTO;
import net.ankao.job.service.IAkJobTaskService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@RestController
@RequestMapping("admin/api/akJobTask")
@MenuScan
public class AkJobTaskController extends AbstractAkController {

    @Resource
    private IAkJobTaskService akJobTaskService;

    @ApiOperation("任务执行记录表")
    @PostMapping("pageList/{jobId}")
    @MenuBelong(name = Constant.TASK_INFO, alias = "执行记录")
    @RequiresPermissions(Constant.TASK_INFO)
    public AkResponse<AkPageable<AkJobTask>> pageList(@RequestBody @Validated JobTaskListDTO jobTaskListDTO, @PathVariable String jobId) {
        AkPageable<AkJobTask> akJobTaskAkPageable = akJobTaskService.pageList(jobId,jobTaskListDTO);
        return AkResponse.SUCCESS(akJobTaskAkPageable);
    }


}
