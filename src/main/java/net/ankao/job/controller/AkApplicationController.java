package net.ankao.job.controller;


import io.swagger.annotations.ApiOperation;
import net.ankao.framework.starter.admin.annotation.MenuBelong;
import net.ankao.framework.starter.admin.annotation.MenuScan;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.common.AkResponse;
import net.ankao.framework.starter.base.controller.AbstractAkController;
import net.ankao.job.common.Constant;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.AkNamespace;
import net.ankao.job.entity.dto.ApplicationAddDTO;
import net.ankao.job.entity.dto.ApplicationListDTO;
import net.ankao.job.entity.dto.ApplicationUpdateDTO;
import net.ankao.job.service.IAkApplicationService;
import net.ankao.job.service.IAkNamespaceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 应用 前端控制器
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@MenuScan
@RestController
@RequestMapping("admin/api/akApplication")
public class AkApplicationController extends AbstractAkController {

    @Resource
    private IAkApplicationService akApplicationService;

    @Resource
    private IAkNamespaceService akNamespaceService;

    @ApiOperation("命名空间列表")
    @PostMapping("namespaceList")
    @RequiresPermissions(Constant.APPLICATION_LIST)
    public AkResponse<List<AkNamespace>> NamespaceList() {
        List<AkNamespace> akJobTaskAkPageable = akNamespaceService.listAll();
        return AkResponse.SUCCESS(akJobTaskAkPageable);
    }

    @ApiOperation("应用详情")
    @GetMapping("info")
    @RequiresPermissions(Constant.APPLICATION_LIST)
    public AkResponse<AkApplication> info(@RequestParam String id) {
        AkApplication akApplication = akApplicationService.getById(id);
        return AkResponse.SUCCESS(akApplication);
    }

    @ApiOperation("删除应用")
    @GetMapping("del")
    @RequiresPermissions(Constant.APPLICATION_UPDATE)
    public AkResponse<Boolean> del(@RequestParam String id) {
        boolean removeById = akApplicationService.del(id);
        return AkResponse.SUCCESS(removeById);
    }

    @ApiOperation("应用列表")
    @PostMapping("pageList/{namespaceId}")
    @MenuBelong(name = Constant.APPLICATION_LIST, alias = "应用列表")
    @RequiresPermissions(Constant.APPLICATION_LIST)
    public AkResponse<AkPageable<AkApplication>> pageList(@PathVariable String namespaceId, @RequestBody @Validated ApplicationListDTO applicationListDTO) {
        AkPageable<AkApplication> akApplicationAkPageable = akApplicationService.pageList(namespaceId, applicationListDTO);
        return AkResponse.SUCCESS(akApplicationAkPageable);
    }

    @ApiOperation("修改")
    @PostMapping("update")
    @MenuBelong(name = Constant.APPLICATION_UPDATE, alias = "应用修改")
    @RequiresPermissions(Constant.APPLICATION_UPDATE)
    public AkResponse<Boolean> update(@RequestBody @Validated ApplicationUpdateDTO applicationUpdateDTO) {
        int akApplication = akApplicationService.updateByDTO(applicationUpdateDTO);
        return AkResponse.SUCCESS(akApplication > 0);
    }


    @ApiOperation("新增")
    @PostMapping("add")
    @RequiresPermissions(Constant.APPLICATION_UPDATE)
    public AkResponse<AkApplication> add(@RequestBody @Validated ApplicationAddDTO applicationAddDTO) {
        AkApplication akApplication = akApplicationService.add(applicationAddDTO);
        return AkResponse.SUCCESS(akApplication);
    }


}
