package net.ankao.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import net.ankao.job.core.common.JobConfig;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.AkApplicationClient;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.dto.ApplicationClientListDTO;

import java.util.List;

/**
 * <p>
 * 应用客户端 服务类
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface IAkApplicationClientService extends IService<AkApplicationClient> {

    /**
     * 上线
     *
     * @param jobConfig
     * @param akApplication
     */
    void onLine(JobConfig jobConfig, AkApplication akApplication);

    /**
     * 下线
     *
     * @param id
     */
    void downUp(String id);

    /**
     * 更新心跳
     *
     * @param id
     */
    void heartbeat(String id);

    /**
     * 根据任务策略获取可用服务节点
     *
     * @param applicationJob
     * @return
     */
    List<AkApplicationClient> getByExecutiveStrategy(AkApplicationJob applicationJob);

    /**
     * 获取所有服务节点，在线的排在前面，线下的排在后面
     *
     * @param applicationClientListDTO
     * @return
     */
    List<AkApplicationClient> list(ApplicationClientListDTO applicationClientListDTO);

    /**
     * 根据应用删除
     * @param applicationId
     * @return
     */
    Integer delByApplicationId(String applicationId);

}
