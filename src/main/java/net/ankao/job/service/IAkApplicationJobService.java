package net.ankao.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.job.core.common.JobInfo;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.dto.JobListDTO;
import net.ankao.job.entity.dto.JobUpdateDTO;

import java.util.List;

/**
 * <p>
 * Job表 服务类
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface IAkApplicationJobService extends IService<AkApplicationJob> {

    /**
     * 初始化job
     *
     * @param jobInfoList
     * @param akApplication
     */
    void init(List<JobInfo> jobInfoList, AkApplication akApplication);

    /**
     * 修改
     *
     * @param jobUpdateDTO
     * @return
     */
    AkApplicationJob updateByDTO(JobUpdateDTO jobUpdateDTO);

    /**
     * 根据状态获取任务列表
     *
     * @param status
     * @return
     */
    List<AkApplicationJob> getListByStatus(Integer status);

    /**
     * 启动
     *
     * @param id
     * @return
     */
    Boolean start(String id);

    /**
     * 关闭
     *
     * @param id
     * @return
     */
    Boolean close(String id);

    /**
     * 分页列表
     *
     * @param jobUpdateDTO
     * @return
     */
    AkPageable<AkApplicationJob> pageList(String applicationId, JobListDTO jobUpdateDTO);

    /**
     * 根据应用删除
     *
     * @param applicationId
     * @return
     */
    Integer delByApplicationId(String applicationId);
}
