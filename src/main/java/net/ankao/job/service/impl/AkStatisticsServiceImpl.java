package net.ankao.job.service.impl;

import net.ankao.job.entity.AkStatistics;
import net.ankao.job.entity.vo.HomeStatisticsVO;
import net.ankao.job.mapper.AkStatisticsMapper;
import net.ankao.job.service.IAkStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 统计数据 服务实现类
 * </p>
 *
 * @author LILU
 * @since 2025-01-17
 */
@Service
public class AkStatisticsServiceImpl extends ServiceImpl<AkStatisticsMapper, AkStatistics> implements IAkStatisticsService {

    @Override
    public HomeStatisticsVO homeStatistics() {
        return this.baseMapper.homeStatistics();
    }

    @Override
    public List<AkStatistics> realTimeData() {
        return this.baseMapper.realTimeData();
    }
}
