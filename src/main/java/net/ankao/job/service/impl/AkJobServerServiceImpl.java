package net.ankao.job.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.ankao.job.core.common.JobConfig;
import net.ankao.job.core.common.JobInfo;
import net.ankao.job.core.rpc.IAkJobServerService;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.service.IAkApplicationClientService;
import net.ankao.job.service.IAkApplicationJobService;
import net.ankao.job.service.IAkApplicationService;
import net.ankao.job.service.IAkJobTaskService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 * @author LILU
 * @date 2024-11-15 9:11
 */
@Slf4j
public class AkJobServerServiceImpl extends UnicastRemoteObject implements IAkJobServerService {

    private IAkApplicationService akApplicationService;

    private IAkApplicationClientService akApplicationClientService;

    private IAkApplicationJobService applicationJobService;

    private IAkJobTaskService akJobTaskService;

//
//    public AkJobServerServiceImpl() throws RemoteException {
//        super();
//
//    }
    public AkJobServerServiceImpl(IAkApplicationService akApplicationService
            , IAkApplicationClientService akApplicationClientService
            , IAkApplicationJobService applicationJobService
            , IAkJobTaskService akJobTaskService) throws RemoteException {
        super();
        this.akApplicationService = akApplicationService;
        this.akApplicationClientService = akApplicationClientService;
        this.applicationJobService = applicationJobService;
        this.akJobTaskService = akJobTaskService;
    }


    @Override
    public void handle(JobConfig jobConfig, List<JobInfo> jobInfoList) throws RemoteException{
        try {
            AkApplication akApplication = akApplicationService.init(jobConfig);
            // 注册节点上线
            akApplicationClientService.onLine(jobConfig, akApplication);
            // 初始化job
            applicationJobService.init(jobInfoList, akApplication);
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
    }

    @Override
    public void callBack(String taskId, Integer status, String msg) throws RemoteException{
        akJobTaskService.updateResult(taskId, status, msg);
    }
}
