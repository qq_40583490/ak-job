package net.ankao.job.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.exception.AkException;
import net.ankao.framework.starter.base.utils.CommonUtil;
import net.ankao.framework.starter.mybatis.plus.utils.AkPageableUtils;
import net.ankao.job.entity.AkNamespace;
import net.ankao.job.entity.dto.NamespaceAddDTO;
import net.ankao.job.entity.dto.NamespaceListDTO;
import net.ankao.job.entity.dto.NamespaceUpdateDTO;
import net.ankao.job.mapper.AkNamespaceMapper;
import net.ankao.job.service.IAkApplicationService;
import net.ankao.job.service.IAkNamespaceService;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 命名空间 服务实现类
 * </p>
 *
 * @author LILU
 * @since 2025-01-16
 */
@Service
public class AkNamespaceServiceImpl extends ServiceImpl<AkNamespaceMapper, AkNamespace> implements IAkNamespaceService {

    @Resource
    private IAkApplicationService akApplicationService;

    @Override
    public AkPageable<AkNamespace> pageList(NamespaceListDTO namespaceListDTO) {
        PageHelper.startPage(namespaceListDTO.getStart(), namespaceListDTO.getLimit());
        List<AkNamespace> akNamespaces = this.baseMapper.infoList(namespaceListDTO);
        return AkPageableUtils.create(akNamespaces);
    }

    @Override
    public List<AkNamespace> listAll() {
        LambdaQueryWrapper<AkNamespace> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.ne(AkNamespace::getId, "public");
        lambdaQueryWrapper.orderByAsc(AkNamespace::getCreateTime);
        List<AkNamespace> applicationJobs = this.baseMapper.selectList(lambdaQueryWrapper);
        return applicationJobs;
    }

    @Override
    public AkNamespace add(NamespaceAddDTO namespaceAddDTO) {
        AkNamespace akNamespace = new AkNamespace();
        if (StringUtils.hasText(namespaceAddDTO.getId())) {
            akNamespace.setId(namespaceAddDTO.getId());
        } else {
            akNamespace.setId(CommonUtil.generateUUID());
        }
        akNamespace.setCreateTime(LocalDateTime.now());
        akNamespace.setName(namespaceAddDTO.getName());
        akNamespace.setNamespaceDes(namespaceAddDTO.getNamespaceDes());
        try {
            this.baseMapper.insert(akNamespace);
            return akNamespace;
        } catch (DuplicateKeyException e) {
            throw new AkException("命名空间已存在");
        }
    }

    @Override
    public int updateByDTO(NamespaceUpdateDTO namespaceUpdateDTO) {
        LambdaUpdateWrapper<AkNamespace> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(AkNamespace::getId, namespaceUpdateDTO.getId());
        lambdaUpdateWrapper.set(AkNamespace::getName, namespaceUpdateDTO.getName());
        lambdaUpdateWrapper.set(AkNamespace::getNamespaceDes, namespaceUpdateDTO.getNamespaceDes());
        return this.baseMapper.update(null, lambdaUpdateWrapper);
    }

    @Override
    public int del(String id) {
        int i = this.baseMapper.deleteById(id);
        if (i > 0) {
            akApplicationService.delByNamespaceId(id);
        }
        return i;
    }

    @Override
    public void initPublic() {
        LambdaQueryWrapper<AkNamespace> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AkNamespace::getId, "public");
        Integer count = this.baseMapper.selectCount(lambdaQueryWrapper);
        if (count == 0) {
            AkNamespace akNamespace = new AkNamespace();
            akNamespace.setId("public");
            akNamespace.setName("public");
            akNamespace.setNamespaceDes("保留空间");
            akNamespace.setCreateTime(LocalDateTime.now());
            this.baseMapper.insert(akNamespace);
        }
    }
}
