package net.ankao.job.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.exception.AkException;
import net.ankao.framework.starter.base.utils.CommonUtil;
import net.ankao.framework.starter.mybatis.plus.utils.AkPageableUtils;
import net.ankao.job.core.common.JobConfig;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.dto.ApplicationAddDTO;
import net.ankao.job.entity.dto.ApplicationListDTO;
import net.ankao.job.entity.dto.ApplicationUpdateDTO;
import net.ankao.job.mapper.AkApplicationMapper;
import net.ankao.job.service.IAkApplicationClientService;
import net.ankao.job.service.IAkApplicationJobService;
import net.ankao.job.service.IAkApplicationService;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 应用 服务实现类
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Service
public class AkApplicationServiceImpl extends ServiceImpl<AkApplicationMapper, AkApplication> implements IAkApplicationService {

    @Resource
    private IAkApplicationJobService akApplicationJobService;

    @Resource
    private IAkApplicationClientService akApplicationClientService;

    @Override
    public AkApplication init(JobConfig jobConfig) {
        AkApplication akApplication = new AkApplication();
        akApplication.setId(CommonUtil.generateUUID());
        akApplication.setCreateTime(LocalDateTime.now());
        akApplication.setApplicationName(jobConfig.getApplicationName());
        akApplication.setApplicationDes(jobConfig.getApplicationDes());
        akApplication.setNamespaceId(jobConfig.getNamespaceId());
        try {
            this.baseMapper.insert(akApplication);
            return akApplication;
        } catch (DuplicateKeyException e) {
            LambdaQueryWrapper<AkApplication> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(AkApplication::getApplicationName, akApplication.getApplicationName());
            AkApplication old = this.baseMapper.selectOne(lambdaQueryWrapper);
            old.setApplicationName(jobConfig.getApplicationName());
            old.setApplicationDes(jobConfig.getApplicationDes());
            this.baseMapper.updateById(old);
            return old;
        }
    }

    @Override
    public Boolean del(String id) {
        int i = this.baseMapper.deleteById(id);
        akApplicationClientService.delByApplicationId(id);
        akApplicationJobService.delByApplicationId(id);
        return i > 0;
    }

    @Override
    public Boolean delByNamespaceId(String namespaceId) {
        LambdaQueryWrapper<AkApplication> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AkApplication::getNamespaceId, namespaceId);
        List<AkApplication> akApplications = this.baseMapper.selectList(lambdaQueryWrapper);
        for (AkApplication akApplication : akApplications) {
            this.del(akApplication.getId());
        }
        return true;
    }

    @Override
    public AkApplication add(ApplicationAddDTO applicationAddDTO) {
        AkApplication akApplication = new AkApplication();
        akApplication.setId(CommonUtil.generateUUID());
        akApplication.setCreateTime(LocalDateTime.now());
        akApplication.setNamespaceId(applicationAddDTO.getNamespaceId());
        akApplication.setApplicationName(applicationAddDTO.getApplicationName());
        akApplication.setApplicationDes(applicationAddDTO.getApplicationDes());
        try {
            this.baseMapper.insert(akApplication);
            return akApplication;
        } catch (DuplicateKeyException e) {
            throw new AkException("应用已存在");
        }
    }

    @Override
    public AkPageable<AkApplication> pageList(String namespaceId, ApplicationListDTO applicationListDTO) {
        PageHelper.startPage(applicationListDTO.getStart(), applicationListDTO.getLimit());
        List<AkApplication> akApplications = this.baseMapper.infoList(namespaceId,applicationListDTO);
        return AkPageableUtils.create(akApplications);
    }

    @Override
    public int updateByDTO(ApplicationUpdateDTO applicationUpdateDTO) {
        LambdaUpdateWrapper<AkApplication> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(AkApplication::getId, applicationUpdateDTO.getId());
        lambdaUpdateWrapper.set(AkApplication::getApplicationName, applicationUpdateDTO.getApplicationName());
        lambdaUpdateWrapper.set(AkApplication::getApplicationDes, applicationUpdateDTO.getApplicationDes());
        return this.baseMapper.update(null, lambdaUpdateWrapper);
    }
}
