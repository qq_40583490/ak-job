package net.ankao.job.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.framework.starter.base.utils.CommonUtil;
import net.ankao.framework.starter.mybatis.plus.utils.AkPageableUtils;
import net.ankao.job.entity.AkApplicationClient;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.AkJobTask;
import net.ankao.job.entity.dto.JobTaskListDTO;
import net.ankao.job.listener.event.SendEmailEvent;
import net.ankao.job.mapper.AkJobTaskMapper;
import net.ankao.job.service.IAkJobTaskService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
@Service
public class AkJobTaskServiceImpl extends ServiceImpl<AkJobTaskMapper, AkJobTask> implements IAkJobTaskService {

    @Resource
    private ApplicationContext applicationContext;

    @Override
    public AkJobTask init(AkApplicationClient akApplicationClient, AkApplicationJob applicationJob, Map<String, String> wrappedMap, String remark) {
        AkJobTask akJobTask = new AkJobTask();
        akJobTask.setId(CommonUtil.generateUUID());
        akJobTask.setJobId(applicationJob.getId());
        akJobTask.setStartTime(LocalDateTime.now());
        akJobTask.setParamsStr(JSONObject.toJSONString(wrappedMap));
        akJobTask.setRemark(remark);
        akJobTask.setNamespaceId(applicationJob.getNamespaceId());
        if (akApplicationClient != null) {
            akJobTask.setClientUrl(akApplicationClient.getClientUrl());
            akJobTask.setClientPort(akApplicationClient.getClientPort());
        }
        this.baseMapper.insert(akJobTask);
        return akJobTask;
    }

    @Override
    public void updateResult(String id, Integer status, String errorLog) {
        LambdaUpdateWrapper<AkJobTask> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(AkJobTask::getId, id);
        lambdaUpdateWrapper.set(AkJobTask::getEndTime, LocalDateTime.now());
        lambdaUpdateWrapper.set(AkJobTask::getStatus, status);
        lambdaUpdateWrapper.set(AkJobTask::getErrorLog, errorLog);
        // 发送邮件
        this.baseMapper.update(null, lambdaUpdateWrapper);
        if(status != 1){
            applicationContext.publishEvent(new SendEmailEvent(this, id));
        }
    }

    @Override
    public AkPageable<AkJobTask> pageList(String jobId, JobTaskListDTO jobTaskListDTO) {
        LambdaQueryWrapper<AkJobTask> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AkJobTask::getJobId, jobId);
        lambdaQueryWrapper.eq(jobTaskListDTO.getStatus() != null, AkJobTask::getStatus, jobTaskListDTO.getStatus());
        lambdaQueryWrapper.ge(jobTaskListDTO.getStartTime() != null, AkJobTask::getStartTime, jobTaskListDTO.getStartTime());
        lambdaQueryWrapper.le(jobTaskListDTO.getEndTime() != null, AkJobTask::getStartTime, jobTaskListDTO.getEndTime());
        lambdaQueryWrapper.like(StringUtils.hasText(jobTaskListDTO.getRemark()), AkJobTask::getRemark, jobTaskListDTO.getRemark());
        lambdaQueryWrapper.orderByDesc(AkJobTask::getStartTime);
        PageHelper.startPage(jobTaskListDTO.getStart(), jobTaskListDTO.getLimit());
        List<AkJobTask> jobTaskList = this.baseMapper.selectList(lambdaQueryWrapper);
        return AkPageableUtils.create(jobTaskList);
    }
}
