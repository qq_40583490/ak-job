package net.ankao.job.service;

import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.job.entity.AkApplicationClient;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.AkJobTask;
import com.baomidou.mybatisplus.extension.service.IService;
import net.ankao.job.entity.dto.JobTaskListDTO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface IAkJobTaskService extends IService<AkJobTask> {

    /**
     * 初始化
     * @param akApplicationClient
     * @param applicationJob
     * @param wrappedMap
     * @param remark
     * @return
     */
    AkJobTask init(AkApplicationClient akApplicationClient, AkApplicationJob applicationJob, Map<String, String> wrappedMap, String remark);

    /**
     * 修改任务结果
     * @param id
     * @param status
     * @param errorLog
     */
    void updateResult(String id,Integer status,String errorLog);

    /**
     * 任务执行结果分页查询
     * @param jobId
     * @param jobTaskListDTO
     * @return
     */
    AkPageable<AkJobTask> pageList(String jobId,JobTaskListDTO jobTaskListDTO);


}
