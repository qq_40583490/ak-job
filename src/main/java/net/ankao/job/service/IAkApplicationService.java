package net.ankao.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.job.core.common.JobConfig;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.AkApplicationJob;
import net.ankao.job.entity.dto.ApplicationAddDTO;
import net.ankao.job.entity.dto.ApplicationListDTO;
import net.ankao.job.entity.dto.ApplicationUpdateDTO;

/**
 * <p>
 * 应用 服务类
 * </p>
 *
 * @author LILU
 * @since 2024-11-15
 */
public interface IAkApplicationService extends IService<AkApplication> {

    /**
     * 初始化应用
     *
     * @param jobConfig
     */
    AkApplication init(JobConfig jobConfig);


    Boolean del(String id);

    /**
     * 根据命名空间删除
     * @param namespaceId
     * @return
     */
    Boolean delByNamespaceId(String namespaceId);

    /**
     * 手动新增应用
     * @param applicationAddDTO
     * @return
     */
    AkApplication add(ApplicationAddDTO applicationAddDTO);

    /**
     * 分页列表
     *
     * @param namespaceId
     * @param applicationListDTO
     * @return
     */
    AkPageable<AkApplication> pageList(String namespaceId,ApplicationListDTO applicationListDTO);

    /**
     * 修改应用
     *
     * @param applicationUpdateDTO
     * @return
     */
    int updateByDTO(ApplicationUpdateDTO applicationUpdateDTO);

}
