package net.ankao.job.service;

import net.ankao.job.entity.AkStatistics;
import com.baomidou.mybatisplus.extension.service.IService;
import net.ankao.job.entity.vo.HomeStatisticsVO;

import java.util.List;

/**
 * <p>
 * 统计数据 服务类
 * </p>
 *
 * @author LILU
 * @since 2025-01-17
 */
public interface IAkStatisticsService extends IService<AkStatistics> {

    /**
     * 统计所有，不区分命名空间
     * @return
     */
    HomeStatisticsVO homeStatistics();


    /**
     * 实时统计数据
     * @return
     */
    List<AkStatistics> realTimeData();


}
