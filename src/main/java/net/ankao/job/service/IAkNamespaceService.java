package net.ankao.job.service;

import net.ankao.framework.starter.base.common.AkPageable;
import net.ankao.job.entity.AkApplication;
import net.ankao.job.entity.AkNamespace;
import com.baomidou.mybatisplus.extension.service.IService;
import net.ankao.job.entity.dto.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 * 命名空间 服务类
 * </p>
 *
 * @author LILU
 * @since 2025-01-16
 */
public interface IAkNamespaceService extends IService<AkNamespace> {

    /**
     * 分页列表
     * @param namespaceListDTO
     * @return
     */
    AkPageable<AkNamespace> pageList(NamespaceListDTO namespaceListDTO);

    /**
     * 全部列表
     * @return
     */
    List<AkNamespace> listAll();

    /**
     * 新增
     * @param namespaceAddDTO
     * @return
     */
    AkNamespace add(NamespaceAddDTO namespaceAddDTO);

    /**
     * 修改
     *
     * @param namespaceUpdateDTO
     * @return
     */
    int updateByDTO(NamespaceUpdateDTO namespaceUpdateDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    int del(String id);

    /**
     * 初始化公共空间
     */
    void initPublic();

}
