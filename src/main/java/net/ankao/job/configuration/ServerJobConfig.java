package net.ankao.job.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author LILU
 * @date 2024-11-14 14:33
 */
@ConfigurationProperties(prefix = "ak.job.server")
public class ServerJobConfig {

    private String url;

    private Integer port;


    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

}
