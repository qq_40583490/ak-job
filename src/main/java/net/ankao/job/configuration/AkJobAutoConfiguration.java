package net.ankao.job.configuration;

import net.ankao.job.common.RmiServerInit;
import net.ankao.job.service.IAkApplicationClientService;
import net.ankao.job.service.IAkApplicationJobService;
import net.ankao.job.service.IAkApplicationService;
import net.ankao.job.service.IAkJobTaskService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

/**
 * 自动装配
 *
 * @author LILU
 * @date 2024-11-14 14:44
 */
@EnableConfigurationProperties({ServerJobConfig.class, EmailConfig.class,AppConfig.class})
public class AkJobAutoConfiguration {

    @Bean
    private RmiServerInit clientInit(ServerJobConfig serverJobConfig, IAkApplicationService akApplicationService
            , IAkApplicationClientService akApplicationClientService, IAkApplicationJobService applicationJobService
            , IAkJobTaskService akJobTaskService) {
        RmiServerInit rmiServerInit = new RmiServerInit();
        rmiServerInit.setClientJobConfig(serverJobConfig);
        rmiServerInit.setAkApplicationService(akApplicationService);
        rmiServerInit.setAkApplicationClientService(akApplicationClientService);
        rmiServerInit.setApplicationJobService(applicationJobService);
        rmiServerInit.setAkJobTaskService(akJobTaskService);
        try {
            rmiServerInit.handle();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return rmiServerInit;
    }


}
