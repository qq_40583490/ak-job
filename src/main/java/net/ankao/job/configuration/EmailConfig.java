package net.ankao.job.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author LILU
 * @date 2025-01-17 16:27
 */
@Data
@ConfigurationProperties(prefix = "ak.job.email")
public class EmailConfig {

    /**
     * 邮箱登录名
     */
    private String loginName;

    /**
     * 授权码/密码
     */
    private String authCode;

    /**
     * SMTP服务器地址 例如：smtp.163.com
     */
    private String smtpHost;



}
