package net.ankao.job.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author LILU
 * @date 2025-01-21 9:23
 */
@Data
@ConfigurationProperties(prefix = "ak.job.app")
public class AppConfig {

    /**
     * 版本号
     */
    private String version;

    /**
     * spring boot版本号
     */
    private String springBootVersion;
}
