package net.ankao.job.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 所属菜单
 *
 * @author LILU
 * @create 2019-02-19 10:22
 * @company
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MenuBelong {
    String name();
    String alias();
}
