package net.ankao.job.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 菜单标记
 * 被标记的类会被扫描
 * @author LILU
 * @create 2019-02-19 10:19
 * @company
 **/
@Documented
@Component
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface MenuScan {

}
