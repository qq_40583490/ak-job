layui.define(['layer'], function (experts) {
    var $ = layui.$
        , layer = layui.layer;

    var Helper = {
        /**
         * 获取顶级窗口
         */
        getTopWindow: function () {
            var topWin = (function (p, c) {
                while (p != c) {
                    c = p
                    p = p.parent
                }
                return c
            })(window.parent, window);
            return topWin;
        }

    };

    function WindowHelper() {
        this.target = Helper.getTopWindow();

        // this.target.external = {};
    }

    WindowHelper.prototype.put = function (key, val) {
        this.target[key] = val;
    }

    WindowHelper.prototype.get = function (key) {
        return this.target[key];
    }

    function LayerPage() {

    }

    LayerPage.prototype.open = function (params) {
        var that = this;

        var dp = {
            path: ""    //子页面路径，可以是带有路由信息
            , title: "" //页面标题
            , iframeSubmitId: ""     //子页面表单提交id
            , btn: ['保存', '取消']
            , success: function (layero, index) {

            }
            , end: function () {

            }
        }

        var options = $.extend({}, dp, params);

        layer.open({
            type: 2
            , title: options.title
            , anim: '0'
            , area: ["40%", "100%"]
            , content: options.path
            , btn: options.btn
            , btnAlign: "c"
            , isOutAnim: false
            , offset: 'r'
            , shade: 0.1
            , success: function (layero, index) {
                // layer.full(index);
                options.success(layero, index);
            }
            , yes: function (index, layero) {
                // var body = layer.getChildFrame('body', index);
                var submit = layero.find("iframe").contents().find(options.iframeSubmitId);
                $(submit).trigger('click');
            }
            , btn2: function (index, layero) {
                if (params.end) {
                    params.end(index, layero);
                    return false;
                }
            }
        })
    }

    /**
     * 关闭页面，在子页面内使用
     */
    LayerPage.prototype.close = function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关
    }

    LayerPage.prototype.load = function () {
        var loadIndex = parent.layer.load(2);
        return new LayerPageLoad(loadIndex, parent)
    }

    function LayerPageLoad(index, parent) {
        this.index = index;
        this.parent = parent;
    }

    LayerPageLoad.prototype.close = function () {
        this.parent.layer.close(this.index);
    }


    function ParentLayerPage2() {
    }

    ParentLayerPage2.prototype = new LayerPage();
    ParentLayerPage2.prototype.open = function (params) {
        var that = this;
        var dp = {
            path: ""    //子页面路径，可以是带有路由信息
            , title: "" //页面标题
            , iframeSubmitId: ""     //子页面表单提交id
            , btn: ['保存', '取消']
            , success: function (layero, index) {

            }, end: function () {

            }
        }

        var options = $.extend({}, dp, params);

        var topWindow = Helper.getTopWindow();
        topWindow.layer.open({
            type: 2
            , title: options.title
            , area: ["70%", "100%"]
            , content: options.path
            , btn: options.btn
            , isOutAnim: false
            , anim: 2
            , shadeClose: true
            , success: function (layero, index) {
                options.success(layero, index);
            }
            , yes: function (index, layero) {
                var submit = layero.find("iframe").contents().find(options.iframeSubmitId);
                $(submit).trigger('click');
            }
            , end: function (index, layero) {
                if (params.end) {
                    params.end();
                }
            }
        })
    };

    experts("AppHelper", {
        getTopWindow: function () {
            return Helper.getTopWindow();
        }
        /**
         * 获取layer的iframe
         * @param index
         * @returns {*}
         */
        , getLayerIframe: function (index) {
            return window['layui-layer-iframe' + index];
        }
        , getWindowHelper: function () {
            return new WindowHelper();
        }
        , put: function (key, val) {
            var window = Helper.getTopWindow();
            window[key] = val;
        }
        , get: function (key) {
            var window = Helper.getTopWindow();
            return window[key];
        }
        , getLayerPage: function () {
            return new LayerPage();
        }
        , getLayerPage2: function () {
            return new ParentLayerPage2();
        }
        , getHost: function () {
            var protocol = window.location.protocol;
            var host = window.location.host;
            return protocol + "//" + host;
        }
    })
})
