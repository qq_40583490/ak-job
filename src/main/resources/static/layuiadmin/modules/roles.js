layui.define(['setter', 'layer', 'form', 'menus'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        menus = layui.menus,
        setter = layui.setter;

    var Methods = {
        add: function (role) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/role"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(role)
            })
        }
        , get: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/role/" + id
            })
        }
        , getFull: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/role/full/" + id
            })
        }
        , update: function (data) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/role/" + data.id
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(data)
            })
        }
        , del: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/role/" + id
                , type: "DELETE"
            })
        }
        ,list:function () {
            return $.ajax({
                url: setter.apiHost + "/admin/api/role/list"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify({start:1,limit:500})
            })
        }
        , getDetail: function (id) {
            var def = $.Deferred();
            $.when($.ajax(setter.apiHost + "/api/role/" + id), $.ajax(setter.apiHost + "/api/menus/all")).done(function (role, menus) {
                def.resolve(role[0], menus[0])
            })
            return def.promise();
        }
    }

    experts("roles", Methods)
})