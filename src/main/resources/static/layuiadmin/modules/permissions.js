layui.define(['setter'], function (experts) {

    var $ = layui.$,
        tableName = "privilege"

    function Permission(user) {
        this.user = user;
        //如果是超级管理员
        if(user.supper){
            layui.data(tableName,{
                key:"supper"
                ,value:true
            })
        }else{
            var menus = user.role.menus;
            var privileges = [];
            $
        }
    }

    Permission.prototype.isSupper = function () {
        var that = this;
        if (that.user.supper) {
            return true;
        } else {
            return false;
        }
    }

    Permission.prototype.getRole = function () {
        var that = this;
        if (that.isSupper()) {
            return "超级管理员";
        }

        if (that.user.role) {
            return that.user.role.roleName;
        } else {
            return "无角色";
        }
    }

    Permission.prototype.has = function (menu) {
        var that = this;
        if (that.isSupper()) {
            return true;
        }

        if (!that.user.role) {
            return false;
        }

        var result = false;
        var menus = that.user.role.menus;
        for (var i in menus) {
            if (menu == menus[i].aliasName) {
                result = true;
                break;
            }
        }
        return result;


    }

    experts('permissions', {
        init:function (user) {
            return new Permission(user);
        }
    })
})