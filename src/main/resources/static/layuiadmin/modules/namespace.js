layui.define(['layer', 'setter', 'roles'], function (exports) {
    var $ = layui.$
        , roles = layui.roles
        , setter = layui.setter;

    var Methods = {
        add: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akNamespace/add"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }, update: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akNamespace/update"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }, info: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akNamespace/info?id=" + id
                , type: "GET"
            })
        }, del: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akNamespace/del?id=" + id
                , type: "POST"
            })
        }
    }

    exports("namespace", {
        methods: Methods
    })
})
