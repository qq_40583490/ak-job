layui.define(function (exports) {
    layui.use(['element'], function () {
        var $ = layui.$;
        var element = layui.element;

        var ProgressBar = function (id, options) {
            var def = {
                big: false
            }

            var params = $.extend(true, {}, def, options);
            var bigStyle = params.big ? "layui-progress-big" : "";

            var eid = new Date().getTime() + "pb";
            var wrap = $('<div class="layui-progress ' + bigStyle + '" lay-showPercent="true" lay-filter="' + eid + '"></div>');
            wrap.append('<div class="layui-progress-bar" lay-percent="0%"></div>');
            $(id).append(wrap);

            element.render("progress")

            return {
                bar: wrap
                , pb: null
                , pbid: eid
                , percent: function (percent) {
                    element.progress(this.pbid, percent);
                }
                , hide: function () {
                    this.bar.css("opacity", 0);
                }
                , show: function () {
                    this.bar.css("opacity", 1);
                }
                , hideAndReset: function () {
                    this.bar.css("opacity", 0);
                    this.percent("0%");
                }
                , remove: function () {
                    var that = this;
                    setTimeout(function () {
                        that.bar.remove();
                    }, 1000)
                }
            }
        }

        exports("progressBar", {
            create: function (id) {
                return new ProgressBar(id);
            }
        })
    })
})