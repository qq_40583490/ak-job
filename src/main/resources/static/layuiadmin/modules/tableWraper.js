layui.define(['table', 'tableSearch'], function (experts) {
    var $ = layui.$
        , table = layui.table
        , tableSearch = layui.tableSearch
    ;
    experts("tableWraper", {
        render: function (config) {
            var defaultConfig = {
                page: true
                , search: true
                , method: 'post'
                , limit: 10
                , text: {
                    none: '暂无数据哦!'
                }
                , contentType: 'application/json'
                , request: {
                    pageName: 'start' //页码的参数名称，默认：page
                    , limitName: 'limit' //每页数据量的参数名，默认：limit
                }
                , parseData: function (res) { //res 即为原始返回的数据
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.message, //解析提示文本
                        "totalCount": res.code == 200 ? res.data.totalCount : 0, //解析数据长度
                        "data": res.code == 200 ? res.data.records:null //解析数据列表
                    };
                }
                , response: {
                    statusName: 'code' //规定数据状态的字段名称，默认：code
                    , statusCode: 200 //规定成功的状态码，默认：0
                    , msgName: 'msg' //规定状态信息的字段名称，默认：msg
                    , countName: 'totalCount' //规定数据总数的字段名称，默认：count
                    , dataName: 'data' //规
                },
            }

            if (config.id == undefined || config.id.length == 0) {
                config.id = config.elem.replace('#', '');
            }
            var params = $.extend(true, {}, defaultConfig, config);
            if (params.search == true) {
                //构造查询对象
                tableSearch.render(params);
            }
            return table.render(params);
        }
    })
})