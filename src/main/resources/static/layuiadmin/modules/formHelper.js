layui.define(function (exports) {
    var $ = layui.$;

//     当你需要对路由结构进行解析时，你只需要通过 layui 内置的方法 layui.router() 即可完成。如上面的路由解析出来的结果是：
//
// {
//     path: ['user','set']
//         ,search: {uid: 123, type: 1}
// ,href: 'user/set/uid=123/type=1'
//     ,hash: 'xxx'
// }
    exports("formHelper", {
        bind: function (filter, object) {
            if(filter == null){
                filter = "";
            }

            layui.each(object, function (key, value) {
                if(value == null){
                    value = "-";
                }
                var name = filter + key;
                var el = $("[id='" + name + "']");
                if(el.size() > 0){
                    var type = el.prop("tagName").toUpperCase();
                    if(type === "SPAN"){
                        el.text(value);
                    }else if(type === "INPUT"){
                        el.val(value);
                    }
                }
            });
        }
    })
})