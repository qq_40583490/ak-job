layui.define(['layer', 'setter'], function (exports) {
    var $ = layui.$
        , setter = layui.setter;

    var Methods = {
        homeStatistics: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akStatistics/homeStatistics"
                , type: "GET"
            })
        }
    }

    exports("statistics", {
        methods: Methods
    })
})
