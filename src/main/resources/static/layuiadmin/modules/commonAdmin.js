layui.define(['layer', 'setter', 'roles'], function (exports) {
    var $ = layui.$
        , roles = layui.roles
        , setter = layui.setter;

    var Methods = {
        add: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/add"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }
        , update: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/update/any"
                , type: "put"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }
        , get: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/" + id
                , type: "GET"
            })
        }
        , del: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/" + id
                , type: "DELETE"
            })
        }
        , resetPasswords: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/resetPasswords"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify({id:id})
            })
        }
        ,profile:function () {
            return $.ajax({
                url: layui.setter.apiHost + '/admin/api/profile'
            })
        }
        ,appInfo:function () {
            return $.ajax({
                url: layui.setter.apiHost + '/admin/api/akStatistics/appInfo'
            })
        }
    }

    exports("commonAdmin", {
        methods: Methods
        , delConfirm: function (id) {
            var def = $.Deferred();
            layer.confirm("删除后不可恢复,您确认要删除该项？", {icon: 3, title: '警告'}, function () {
                var delIndex = layer.load(2);
                Methods.del(id).done(function (res) {
                    layer.msg("删除成功", {icon: 1});
                    def.resolve(res);
                }).always(function () {
                    layer.close(delIndex);
                })
            })

            return def.promise();
        }
        , resetPasswordsConfirm: function (id) {
            var def = $.Deferred();
            layer.confirm("重置后密码为‘123456’，您确认要重置吗？", {icon: 3, title: '警告'}, function () {
                var delIndex = layer.load(2);
                Methods.resetPasswords(id).done(function (res) {
                    layer.msg("重置成功", {icon: 1});
                    def.resolve(res);
                }).always(function () {
                    layer.close(delIndex);
                })
            })

            return def.promise();
        }
    })
})