layui.define(['layer', 'upload', 'setter'], function (experts) {
    var $ = layui.$,
        upload = layui.upload
        , layer = layui.layer
    ;

    function OssUploader(params) {
        var that = this;
        this.config = {
            elem: ""
            , auto: true    //自动渲染预览
            , accept: "images"
            , acceptMime: "image/*"
            , exts: "jpg|png|gif|bmp|jpeg"
            , val: null  //默认值
            , url: null
            , before: function () {

            }
            , done: function (url,file) {

            }
            , error: function () {

            }
        }

        var options = $.extend({}, this.config, params);
        if ($(options.elem).val()) {
            options.val = $(options.elem).val();
        }


        var box = $('<div class="upload-box"></div>');
        var preview = $('<div class="preview-img-box"></div>');
        var previewTxt = $('<div><i class="layui-icon layui-icon-picture-fine" style="font-size: 40px;"></i><div class="doc-icon-name">预览</div></div>');
        var previewLoading = $('<div class="upload-preview-loading" style="display: none;"><i class="layui-icon layui-icon-loading-1 layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 40px;"></i></div>');

        //渲染页面dom
        that.render = function () {
            $(options.elem).wrap(box);
            preview.append(previewTxt);
            preview.append(previewLoading);
            $(options.elem).before(preview);
            $(options.elem).after('<span style="margin-left: 20px;">注：仅支持'+options.exts+'格式</span>');
        }

        //回显
        that.preview = function (val) {
            if (val) {
                preview.css("background-image", "url('" + val + "')");
                previewTxt.hide();
            }
        }

        var isLoading = false;
        that.loadingStatus = function () {
            $(options.elem).addClass("layui-btn-disabled");
            previewLoading.show();
            isLoading = true;
        }

        that.cancelLoadingStatus = function () {
            $(options.elem).removeClass("layui-btn-disabled");
            previewLoading.hide();
            isLoading = false;
        }

        that.reset = function () {
            if (options.val) {
                that.preview(options.val);
            } else {
                previewTxt.show();
            }
        }

        if (options.auto) {
            that.render(options.val);
            that.preview(options.val);
        }

        var currFile = null;
        upload.render({
            elem: options.elem
            , accept: options.accept
            , acceptMime: options.acceptMime
            , url: options.url
            , type: "post"
            , exts: options.exts
            , choose: function (obj) {
                if (isLoading) {
                    layer.alert("正在上传,请稍后操作!");
                    return;
                }

                options.before();

                that.loadingStatus();
                previewTxt.hide();

                currFile = obj.file;
            }
            , done: function (res, index, upload) {
                //获取当前触发上传的元素，一般用于 elem 绑定 class 的情况，注意：此乃 layui 2.1.0 新增
                var item = this.item;
                var code = res.code;
                if (code != "100000") {
                    layer.alert("上传失败!", {icon: 0})
                    options.error();
                    that.cancelLoadingStatus();
                    that.reset();
                    return;
                }

                var publicUrl = res.data.publicUrl;

                that.cancelLoadingStatus();
                //回显预览
                if (options.auto) {
                    that.preview(publicUrl)
                }

                options.done(publicUrl, currFile);
            }
        })
    }


    experts("normalUploader", OssUploader)
}).link(layui.setter.base + "/modules/ossUploader.css")