layui.define(function (experts) {
    function EventHandler(options) {
        var that = this;
        that.constructor = EventHandler;
        that.eventHandlers = {};
    }

    //注册事件监听
    EventHandler.prototype.on = function (eventName, handler) {
        var that = this;
        if (typeof that.eventHandlers[eventName] == "undefined") {
            that.eventHandlers[eventName] = [];
        }

        that.eventHandlers[eventName].push(handler);
    }

    //触发事件
    EventHandler.prototype.fire = function (eventName, data) {
        var that = this;
        if (that.eventHandlers[eventName] instanceof Array) {
            var handlers = that.eventHandlers[eventName];
            for (var i in handlers) {
                handlers[i](data);
            }
        }
    }

    //删除事件
    EventHandler.prototype.removeEvent = function (eventName, handler) {
        var that = this;
        if (that.eventHandlers[eventName] instanceof Array) {
            var handlers = that.eventHandlers[eventName];
            for (var i in handlers) {
                if (handlers[i] === handlers) {
                    handlers.splice(i, 1);
                    break;
                }
            }
        }
    }
    experts("eventHandler", EventHandler);
})