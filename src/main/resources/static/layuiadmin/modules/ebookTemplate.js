layui.define(['setter', 'layer', 'form', 'upload', 'AppHelper', 'routerHelper', 'auth'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        upload = layui.upload,
        routerHelper = layui.routerHelper,
        AppHelper = layui.AppHelper,
        baseUri = "/admin/api/ebook/template",
        setter = layui.setter;

    experts("ebookTemplate", {
        addTemplate: function (ebookTemplate) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/add"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(ebookTemplate)
            })
        },
        getTemplates: function () {
            return $.ajax({
                url: setter.apiHost + baseUri + "/all"
                , type: "GET"
            })
        },
        getTemplateById: function (id) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/" + id
                , type: "GET"
            })
        },
        updateTemplateById: function (id, ebookTemplate) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/update/" + id
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(ebookTemplate)

            })
        },
    })
})