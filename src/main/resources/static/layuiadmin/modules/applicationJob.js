layui.define(['layer', 'setter', 'roles'], function (exports) {
    var $ = layui.$
        , roles = layui.roles
        , setter = layui.setter;

    var Methods = {
        execute: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationJob/execute"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }, update: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationJob/update"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }, info: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationJob/info?id=" + id
                , type: "GET"
            })
        }, start: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationJob/start?id=" + id
                , type: "GET"
            })
        }, close: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationJob/close?id=" + id
                , type: "GET"
            })
        },  clientList: function (applicationId) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationJob/list"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify({applicationId:applicationId})
            })
        }
    }

    exports("applicationJob", {
        methods: Methods
        , delConfirm: function (id) {
            var def = $.Deferred();
            layer.confirm("您确认要删除该应用吗？删除后不可恢复", {icon: 3, title: '警告'}, function () {
                var delIndex = layer.load(2);
                Methods.del(id).done(function (res) {
                    layer.msg("删除成功", {icon: 1});
                    def.resolve(res);
                }).always(function () {
                    layer.close(delIndex);
                })
            })

            return def.promise();
        }
    })
})
