layui.define(['index', 'layer', 'jquery', 'setter','routerHelper'], function (exports) {

    var setter = layui.setter;
    var request = setter.request;
    var $ = layui.$;

    var obj={
        ajaxPostPromise:function (uri,inParam) {
            var dtd=$.Deferred();
            $.ajax({
                url:layui.setter.apiHost+uri,
                method:'POST',
                dataType:'json',
                data:inParam,
                success:function (data) {
                    dtd.resolve(data);
                },
                error:function (data) {
                    dtd.reject(data);
                }
            });
            return dtd;
        },
        ajaxGetPromise:function (uri,inParam) {
            var dtd=$.Deferred();
            if(inParam==null){
                uri = uri+"?token="+ (layui.data(setter.tableName)[request.tokenName] || '');
            }else{
                var p = "";
                for(var s in inParam) {
                    if(p==""){
                        p+="?"+s+"="+inParam[s];
                    }else{
                        p+="&"+s+"="+inParam[s];
                    }

                }
                uri += p+"&token="+ (layui.data(setter.tableName)[request.tokenName] || '');
            }

            $.ajax({
                url:layui.setter.apiHost+uri,
                method:'GET',
                dataType:'json',
                success:function (data) {
                    dtd.resolve(data);
                },
                error:function (data) {
                    console.log("err:"+uri);
                    dtd.reject(data);
                }
            });
            return dtd;
        }
    };
    exports('myAjax', obj);
});

