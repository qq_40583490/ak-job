layui.define(function (exports) {
    exports("global", {

        parseData: function (res) { //res 即为原始返回的数据
            console.log(res)
            return {
                "status": res.status, //解析接口状态
                "msg": res.message, //解析提示文本
                "total": res.data.total, //解析数据长度
                "data": res.data //解析数据列表
            };
        }
        , response: {
            statusName: 'status' //规定数据状态的字段名称，默认：code
            , statusCode: 200 //规定成功的状态码，默认：0
            , msgName: 'msg' //规定状态信息的字段名称，默认：msg
            , countName: 'total' //规定数据总数的字段名称，默认：count
            , dataName: 'data' //规
        }
    })
})