layui.define(function (experts) {
    experts('simpleTool', {
        fileHost:function (url) {
          return layui.setter.fileHost+"/"+url;
        },

        length1:function (uname) {

            if(uname==null||uname.length==1) {
                return uname;
            }else{
                return uname.substr(0,1)+uname.substr(1).replace(/[\s\S]/g,'*');
            }
        },

        phone4star:function (uname) {

            if(uname==null||uname.length==1) {
                return uname;
            }else{
                return uname.substr(0,3)+  (uname.substr(3,4).replace(/[\s\S]/g,'*'))  +uname.substr(7);
            }
        }
        ,
        keyupLegalCode:function (ts) {
            ts.value=ts.value.replace(/[^A-Z0-9]/g,'');
        }
        ,
        ip:function (ts) {
            ts.value=ts.value.replace(/[^.0-9]/g,'');
        }
        ,
        ips:function (ts) {
            ts.value=ts.value.replace(/[^.,0-9]/g,'');
        }
        ,
        keyupPhone:function (ts) {

            ts.value=ts.value.replace(/[^0-9]/g,'');
        }
        ,
        keyupAmount:function (ts) {
            ts.value=ts.value.replace(/[^0-9.]/g,'');
        }
        ,
        keyupPersonName:function (ts) {
            ts.value=ts.value.replace(/[^0-9\\u4e00-9fa5]/g,'');
        }
        ,
        translateStatusToCn:function(theStatus) {
            console.log('状态status='+theStatus);
            if (theStatus == 'review_draft') {
                $("#status").text('未提交');
            }
            if (theStatus == 'review') {
                $("#status").text('等待审核');
            }
            if (theStatus == 'review_pass') {
                $("#status").text('审核通过');
            }
            if (theStatus == 'review_fail') {
                $("#status").text('审核不通过');
            }
        }
    });
});