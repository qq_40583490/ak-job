layui.define(['setter', 'layer', 'form'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        setter = layui.setter;

    experts("announcement", {
        findById: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/announcement/" + id
                , type: "GET"
            })
        }
        , addAnnouncement: function (announcement) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/announcement"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(announcement)
            })
        }
        , updateAnnouncement: function (id, announcement) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/announcement/" + id
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(announcement)
            })
        }
        , del: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/announcement/" + id
                , type: "DELETE"
            })
        }
    })
})