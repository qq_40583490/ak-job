layui.define(function (exports) {
    var $ = layui.$;

//     当你需要对路由结构进行解析时，你只需要通过 layui 内置的方法 layui.router() 即可完成。如上面的路由解析出来的结果是：
//
// {
//     path: ['user','set']
//         ,search: {uid: 123, type: 1}
// ,href: 'user/set/uid=123/type=1'
//     ,hash: 'xxx'
// }
    exports("routerHelper", {
        /**
         * 拼装页面跳转参数，query里面传递参数对象就行
         *
         * 在跳转后的页面直接用layui 内置的方法 layui.router()，即可获取对应参数:
         * {
         * path: ['user','set']
         * ,search: {uid: 123, type: 1}
         * ,href: 'user/set/uid=123/type=1'
         * ,hash: 'xxx'
         * }
         * 其中search就是query参数
         * @param params
         * @returns {string}
         */
        jump: function (params) {
            var defParams = {
                path: ""
                , query: {}
            }

            var options = $.extend({}, defParams, params);

            var newPath = options.path + "#/";

            for (var key in options.query) {
                newPath += key + "=" + options.query[key] + "/";
            }

            return newPath.substring(0, newPath.length - 1);

        }
    })
})