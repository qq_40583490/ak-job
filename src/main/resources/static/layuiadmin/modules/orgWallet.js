layui.define(['setter', 'layer', 'form', 'menus'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        menus = layui.menus,
        baseUri = "/api/organization/role",
        setter = layui.setter;


    experts("orgWallet", {
        vipCards: {
            request: {
                /**
                 * 绑定会员卡
                 * @param cardId
                 * @param userId
                 */
                bindCard: function (cardId, userId) {
                    var layerIndex = layer.load(2);
                    return $.ajax({
                        url: setter.apiHost + "/api/organization/wallet/vip_card/" + cardId + "/bind"
                        , type: "put"
                        , data: {
                            userId: userId
                        }
                    }).always(function () {
                        layer.close(layerIndex);
                    })
                }
                ,unbindCard:function (cardId) {
                    var layerIndex = layer.load(2);
                    return $.ajax({
                        url: setter.apiHost + "/api/organization/wallet/vip_card/" + cardId + "/unbind"
                        , type: "put"
                    }).always(function () {
                        layer.close(layerIndex);
                    })
                }
            }
        }
    })
})