layui.define(['setter', 'layer', 'form', 'upload', 'AppHelper', 'routerHelper', 'auth'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        upload = layui.upload,
        routerHelper = layui.routerHelper,
        AppHelper = layui.AppHelper,
        exhibitionTypeUri = "/admin/api/exhibition/category",
        setter = layui.setter;

    experts("exhibitionType",

        {
            add: function (exhibition) {
                return $.ajax({
                    url: setter.apiHost + exhibitionTypeUri + "/add"
                    , type: "POST"
                    , contentType: "application/json"
                    , data: JSON.stringify(exhibition)
                })
            },
            getById: function (id) {
                return $.ajax({
                    url: setter.apiHost + exhibitionTypeUri + "/" + id
                    , type: "GET"
                })
            },
            update: function (id, exhibition) {
                return $.ajax({
                    url: setter.apiHost + exhibitionTypeUri + "/update/" + id
                    , type: "PUT"
                    , contentType: "application/json"
                    , data: JSON.stringify(exhibition)

                })
            }
            ,del: function (id) {
                return $.ajax({
                    url: setter.apiHost + exhibitionTypeUri + "/del/" + id
                    , type: "delete"
                    , contentType: "application/json"
                })
            }
        }
    )
})