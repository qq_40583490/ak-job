layui.define(function (exports) {
    var $ = layui.$;
    var Helper = function (url) {
        var baseUrl = url.split("?")[0];


        return {
            getBase: function () {
                return baseUrl;
            }
            , getThumbnailUrl: function (options) {
                var dfp = {
                    width: "300"
                    , height: "300"
                    , quality: "90"
                    , format: "jpg"
                }

                var params = $.extend(true, {}, dfp, options);

                var queryParams = "?imageMogr2/auto-orient/thumbnail/"
                    + params.width + "x" + params.height +
                    "/strip/quality/" + params.quality +
                    "/format/" + params.format;

                return baseUrl + queryParams;
            }
        }
    }

    exports("qiniuHelper", {
        getHelper:function (url) {
            return new Helper(url);
        }
    })
})