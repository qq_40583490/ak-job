layui.define(function (exports) {
    var $ = layui.$;
    var host = document.domain;
    var setter = layui.setter;
    $.getScript(layui.setter.base + "/3prt/store.min.js", function () {
        var Storage = {
            saveToken: function (val) {
                //请求成功后，写入 access_token
                layui.data(setter.tableName, {
                    key: setter.request.tokenName
                    , value:val
                });
            },
            getToken: function () {
                return (layui.data(setter.tableName)[setter.request.tokenName] || '');
            },
            getCookie: function (key) {
                var items = document.cookie.split(";");
                var value = "";
                items.forEach(function (item) {
                    var key = $.trim(item).split("=");
                    if ($.trim(key[0]) == key[0]) {
                        value = $.trim(key[1]);
                    }
                });

                return value;
            },
            getCompareList: function () {
                var map = store.get("compareList");
                var token = map ? map[host] : "";
                return token;
            },
            setCompareList: function (val) {
                var map = {};
                map[host] = val;
                store.set("compareList", map);
            }
            , set: function (key, val) {
                store.set(key, val);
            }
            , get: function (key) {
                return store.get(key);
            }
            , setProjectItemId: function (projectItemId) {
                store.set("projectItemId", projectItemId);
            }
            , getProjectItemId: function () {
                return store.get("projectItemId");
            }, setExpertApplyId: function (expertApplyId) {
                store.set("expertApplyId", expertApplyId);
            }
            , getExpertApplyId: function () {
                return store.get("expertApplyId");
            },
            saveRole: function (val) {
                store.set("role", val);
            },
            getRole: function () {
                console.log("role:", store.get("role"))
                return store.get("role");
            }
            , setRoleByProjectItem: function (role, projectItemId) {
                store.set(projectItemId, role);
            }
            , getRoleByProjectItem: function (projectItemId) {
                return store.get(projectItemId);
            }
            , getChoiceList: function () {
                var map = store.get("choiceList");
                return map;
            }
            , getChoice: function () {
                return store.get("choice");
            }
        }
        exports("storage", Storage)
    })
})