layui.define(['layer', 'setter', 'roles'], function (exports) {
    var $ = layui.$
        , roles = layui.roles
        , setter = layui.setter;

    var Methods = {
        pwd: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/pwd"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }
    }

    exports("home", {
        methods: Methods
    })
})
