layui.define(function (exports) {
    var $ = layui.$;

    var Advice = function (params) {
        var ue = UE.getEditor(params.containerId, {
            initialFrameWidth: params.width,//宽度
            initialFrameHeight: params.height//高度
            , zIndex: 10
        });

        //内容改变回调
        ue.addListener("contentchange", function () {
            params.onchange(ue.getContent());
        });
        ue.ready(function () {
            ue.setContent(params.content);
        });

        ue.ready(function () {
            // 监听插入图片
            // ue.addListener("beforeInsertImage", function (o,o1,o2,o3) {
            //     console.log(o1)
            //     console.log(o2)
            //     console.log(o3)
            // });
            // 监听插入附件
            // ue.addListener("afterUpfile",_afterUpfile);
        });
        return ue;
    }

    var getParams = function (options) {
        var defParams = {
            containerId: "container"
            , width: "100%"
            , height: 700
            , content: ""
            , onchange: function (content) {

            }
        }
        return $.extend({}, defParams, options);
    }

    exports("ueditorAdvice", {
        simple: function (options) {
            var params = getParams(options);
            return new Advice(params);
        }
    })
})