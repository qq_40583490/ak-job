layui.define(['setter', 'layer', 'form'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        setter = layui.setter;

    var Methods = {
        add: function (menus) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(menus)
            })
        }
        , get: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/" + id
            })
        }
        , update: function (data) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/" + data.id
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(data)
            })
        }
        , drag: function (dragMenus) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/drag"
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(dragMenus)
            })
        }
        , del: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/custom/" + id
                , type: "DELETE"
            })
        }
        , delCustom: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/custom/" + id
                , type: "DELETE"
            })
        }
        , all: function () {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/list"
                , type: "GET"
            })
        }
        , allFeatures: function () {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/feature/list"
                , type: "GET"
            })
        }
        , notUsedFeatures: function () {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/feature/not_used/list"
                , type: "GET"
            })
        }
        , usedFeatures: function (menuId) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/" + menuId + "/feature/list"
                , type: "GET"
            })
        }
        , updateUsed: function (menuId, featureIds) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/menus/" + menuId + "/feature"
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(featureIds)
            })
        }
    }

    experts("menus", {
        methods: Methods
        , addForm: function () {
            var that = this;
            var def = $.Deferred();
            layer.open({
                type: 1
                , title: "添加菜单"
                , area: "500px"
                , content: juicer("#menusForm", {})
                , success: function (layero, index) {
                    form.on("submit(menusFormSubmit)", function (obj) {
                        var field = obj.field;
                        var loadindex = layer.load();
                        Methods.add(field).done(function (res) {
                            if (res.code == "100000") {
                                layer.msg("保存成功");
                                layer.close(index);
                                def.resolve(res);
                            }
                        }).always(function () {
                            layer.close(loadindex);
                        })
                    })
                }
            })
            return def.promise();
        }
        , editForm: function (id) {
            var that = this;
            var openIndex = layer.load(2);
            var def = $.Deferred();
            Methods.get(id).done(function (res) {
                var data = res.data;
                layer.open({
                    type: 1
                    , title: "编辑菜单"
                    , area: "500px"
                    , content: juicer("#menusForm", data)
                    , success: function (layero, index) {
                        form.on("submit(menusFormSubmit)", function (obj) {
                            var field = obj.field;
                            field.id = id;
                            var saveIndex = layer.load();
                            Methods.update(field).done(function (res) {
                                if (res.code == "100000") {
                                    layer.msg("保存成功");
                                    layer.close(index);
                                    def.resolve(res);
                                }
                            }).always(function () {
                                layer.close(saveIndex);
                            })
                        })
                    }
                })
            }).always(function () {
                layer.close(openIndex);
            })
            return def.promise();
        }
        , delConfirm: function (id) {
            var def = $.Deferred();
            layer.confirm("您确认要删除该项？删除后不可恢复", {icon: 3, title: '警告'}, function () {
                var delIndex = layer.load(2);
                Methods.del(id).done(function (res) {
                    layer.alert("删除成功");
                    def.resolve(res);
                }).always(function () {
                    layer.close(delIndex);
                })
            })

            return def.promise();
        }
        , renderTree: function (data) {

        }
    })
})