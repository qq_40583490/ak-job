layui.define(['layer', 'setter', 'roles'], function (exports) {
    var $ = layui.$
        , roles = layui.roles
        , setter = layui.setter;

    var Methods = {
        add: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplication/add"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }, update: function (admin) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplication/update"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(admin)
            })
        }, info: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplication/info?id=" + id
                , type: "GET"
            })
        }, del: function (id) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplication/del?id=" + id
                , type: "GET"
            })
        }, clientList: function (applicationId) {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplicationClient/list"
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify({applicationId:applicationId})
            })
        }, namespaceList: function () {
            return $.ajax({
                url: setter.apiHost + "/admin/api/akApplication/namespaceList"
                , type: "POST"
                , contentType: "application/json"
            })
        }
    }

    exports("application", {
        methods: Methods
        , delConfirm: function (id) {
            var def = $.Deferred();
            layer.confirm("您确认要删除该应用吗？删除后不可恢复", {icon: 3, title: '警告'}, function () {
                var delIndex = layer.load(2);
                Methods.del(id).done(function (res) {
                    layer.msg("删除成功", {icon: 1});
                    def.resolve(res);
                }).always(function () {
                    layer.close(delIndex);
                })
            })

            return def.promise();
        }
    })
})
