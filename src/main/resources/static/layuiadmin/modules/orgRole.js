layui.define(['setter', 'layer', 'form', 'menus'], function (experts) {
    var $ = layui.$,
        layer = layui.layer,
        form = layui.form,
        menus = layui.menus,
        baseUri = "/api/organization/role",
        setter = layui.setter;

    var Methods = {
        add: function (role) {
            return $.ajax({
                url: setter.apiHost + baseUri
                , type: "POST"
                , contentType: "application/json"
                , data: JSON.stringify(role)
            })
        }
        , getFull: function (id) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/full/" + id
            })
        }
        , update: function (data) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/" + data.id
                , type: "PUT"
                , contentType: "application/json"
                , data: JSON.stringify(data)
            })
        }
        , del: function (id) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/" + id
                , type: "DELETE"
            })
        }
        , all: function () {
            return $.ajax({
                url: setter.apiHost + baseUri + "/all"
                , type: "GET"
            })
        }
        ,bindRole:function (userId, roleId) {
            return $.ajax({
                url: setter.apiHost + baseUri + "/bind"
                , type: "PUT"
                ,data:{
                    userId:userId
                    ,roleId:roleId
                }
            })
        }
    }

    experts("orgRole", Methods)
})